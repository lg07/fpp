﻿using UnityEngine;
using NUnit.Framework;
using Localization;
using UnityEngine.UI;
using UnityEditor;

public class LocalizationTests
{
    [Test]
    public void CreateLocalizationKeyTest()
    {
        bool keysAdded = true;

        GameObject test = new GameObject();
        test.AddComponent<Text>();
        test.AddComponent<LocalizedText>();

        test.GetComponent<Text>().text = "test010203_A";
        test.GetComponent<LocalizedText>().CreateNewLocalizationKey();

        AssetDatabase.Refresh();

        GameData gameManager = AssetDatabase.LoadAssetAtPath<GameData>("Assets/Resources/GameManager.prefab");
        LocalizationManager localizationManager = gameManager.gameObject.GetComponent<LocalizationManager>();

        foreach (var localizationFile in localizationManager.localizationAssets[0].localizationFiles)
        {
            LocalizationList localizationList = JsonUtility.FromJson<LocalizationList>(localizationFile.text);

            for (int i = 0; i < localizationList.localizationObjects.Count; i++)
            {
                if (localizationList.localizationObjects[i].text == "test010203_A")
                {
                    break;
                }
                else if (i == localizationList.localizationObjects.Count - 1)
                {
                    keysAdded = false;
                }
            }
        }

        Assert.True(keysAdded);

        //Cleaning
        test.GetComponent<LocalizedText>().DeleteLocalizationKey();
        GameObject.DestroyImmediate(test);
    }

    [Test]
    public void DeleteLocalizationKeyTest()
    {
        bool keysRemoved = true;

        GameObject test = new GameObject();
        test.AddComponent<Text>();
        test.AddComponent<LocalizedText>();

        test.GetComponent<Text>().text = "test010203_A";
        test.GetComponent<LocalizedText>().CreateNewLocalizationKey();

        AssetDatabase.Refresh();

        test.GetComponent<LocalizedText>().DeleteLocalizationKey();

        AssetDatabase.Refresh();

        GameData gameManager = AssetDatabase.LoadAssetAtPath<GameData>("Assets/Resources/GameManager.prefab");
        LocalizationManager localizationManager = gameManager.gameObject.GetComponent<LocalizationManager>();

        foreach (var localizationFile in localizationManager.localizationAssets[0].localizationFiles)
        {
            LocalizationList localizationList = JsonUtility.FromJson<LocalizationList>(localizationFile.text);

            for (int i = 0; i < localizationList.localizationObjects.Count; i++)
            {
                if (localizationList.localizationObjects[i].text == "test010203_A")
                {
                    keysRemoved = false;
                    break;
                }
            }
        }

        Assert.True(keysRemoved);

        //Cleaning
        GameObject.DestroyImmediate(test);
    }
}
