﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LocalizedText))]
public class LocalizationUpdaterEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        LocalizedText localizedText = (LocalizedText)target;
        if (GUILayout.Button("Create Localization Key"))
        {
            localizedText.CreateNewLocalizationKey();
        }
        if (GUILayout.Button("Delete Localization Key"))
        {
            localizedText.DeleteLocalizationKey();
        }
    }
}
