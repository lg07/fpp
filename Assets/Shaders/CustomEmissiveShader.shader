﻿Shader "Custom/CustomEmissiveShader"
{
    Properties
    {
        _MainTex ("Main Texture", 2D) = "white" {}
		_EmissionColor ("Emission Color", Color) = (0.000000,0.000000,0.000000,1.000000)
		_EmissionMap ("Emission", 2D) = "white" { }
		_EmissionMultiplier ("Emission Multiplier", Range(0,5)) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
        };

		fixed4 _EmissionColor;
		sampler2D _EmissionMap;
		half _EmissionMultiplier;

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
            o.Albedo = c.rgb;
			o.Emission = tex2D(_EmissionMap, IN.uv_MainTex).rgb * (_EmissionColor * _EmissionMultiplier).rgb;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
