﻿using UnityEngine;

public class Initializer : MonoBehaviour
{
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    static void OnBeforeSceneLoadRuntimeMethod()
    {
        GameObject gameManagerObject = Resources.Load("GameManager") as GameObject;
        GameObject gameManager = Instantiate(gameManagerObject) as GameObject;
    }
}