﻿using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;

public class SaveManager : MonoBehaviour
{
    [HideInInspector]
    public static int[] bestTimes;

    static string saveFilePath = "";

    static void CreateNewData()
    {
        bestTimes = new int[5]; //TODO how many levels? Solution: counting level scenes?
        for (int i=0; i<bestTimes.Length; i++)
        {
            bestTimes[i] = 0;
        }

        SaveData();
    }

    public static void SaveData()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(saveFilePath);
        SaveData sd = new SaveData();

        //TODO UI save indicator

        sd.bestTimes = bestTimes;

        bf.Serialize(file, sd);
        file.Close();
    }

    public static void LoadData()
    {
        if (File.Exists(saveFilePath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(saveFilePath, FileMode.Open);
            try
            {
                SaveData sd = (SaveData)bf.Deserialize(file);
                file.Close();

                bestTimes = sd.bestTimes;
            }

            catch (Exception e)
            {
                Debug.Log("Corrupted save file! Replacing with empty data...");
                file.Close();
                File.Delete(saveFilePath);
                CreateNewData();
            }
        }

        else
        {
            CreateNewData();
        }
    }

    //TODO ClearData()

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    static void Initialize ()
    {
        if (Application.isEditor)
            saveFilePath = Application.persistentDataPath + "/saved.dat";
        else
            saveFilePath = Application.dataPath + "/saved.dat";

        LoadData();
    }
}
