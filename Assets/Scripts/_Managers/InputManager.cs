﻿using UnityEngine;
using System;

public class InputManager : SingletonBase<InputManager>
{
    bool gamepadOn = false;
    public bool GamepadOn { get { return gamepadOn; } }

    [HideInInspector]
    public float gamepadSensitivity = 50;
    [HideInInspector]
    public float mouseSensitivity = 50;

    bool playerInputActive = true;
    public bool PlayerInputActive { get { return playerInputActive; } }

    public static event Action<bool> GamepadStateChangedEvent;
    public void GamepadStateChanged(bool state)
    {
        if (GamepadStateChangedEvent != null) GamepadStateChangedEvent(state);
    }

    void ChangeGamepadState(bool gamepadOn)
    {
        if (gamepadOn)
        {
            this.gamepadOn = true;
            GamepadStateChanged(true);
        }
        else
        {
            this.gamepadOn = false;
            GamepadStateChanged(false);
        }
    }

    public void SetPlayerInput(bool active)
    {
        playerInputActive = active;
    }

    void Awake ()
    {   
        ChangeGamepadState(Convert.ToBoolean(PlayerPrefs.GetInt(GameData.gamepadState)));
        gamepadSensitivity = PlayerPrefs.GetFloat(GameData.gamepadSensitivity);
        mouseSensitivity = PlayerPrefs.GetFloat(GameData.mouseSensitivity);
    }
	
	void Update ()
    {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");
        float gamepadLookX = Input.GetAxis("PadLookX");
        float gamepadLookY = Input.GetAxis("PadLookY");
        float gamepadHorizontal = Input.GetAxis("GamepadHorizontal");
        float gamepadVertical = Input.GetAxis("GamepadVertical");

        if (gamepadOn && (Mathf.Abs(mouseX) > 0.01f || Mathf.Abs(mouseY) > 0.01f)) //TODO detect keyboard
        {
            ChangeGamepadState(false);
        }
        else if (!gamepadOn && ((Mathf.Abs(gamepadLookX) > 0.01f || Mathf.Abs(gamepadLookY) > 0.01f) 
            || Mathf.Abs(gamepadHorizontal) > 0.01f || Mathf.Abs(gamepadVertical) > 0.01f || Input.GetAxis("Fire2") > 0)) //TODO detect other gamepad buttons
        {
            ChangeGamepadState(true);
        }
    }
}
