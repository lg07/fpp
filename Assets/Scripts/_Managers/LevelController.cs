﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

//Handles current level gameplay state
public class LevelController : SingletonBase<LevelController>
{
    bool gameOver;
    public bool GameOver { get { return gameOver; } }
    bool victory;
    public bool Victory { get { return victory; } }

    [SerializeField]
    [Tooltip("How long should Blue state last (seconds)")]
    int blueStateDuration = 15;

    public enum LevelState { BLUE, RED };
    LevelState levelState = LevelState.RED;
    public LevelState GetLevelState { get { return levelState; } }

    public enum PickUpType { STATE, COLLECTIBLE };

    WaitForSeconds oneSecond;

    [HideInInspector]
    public int elapsedTime;

    GameObject lastRemainingPickup = null;
    GameObject player = null;

    //EVENTS---
    public static event Action<LevelState> SwitchLevelStateEvent;
    void SwitchLevelState(LevelState state)
    {
        if (SwitchLevelStateEvent != null) SwitchLevelStateEvent(state);
        levelState = state;
    }

    public static event Action<int, int> ProgressReportEvent;
    void ProgressReport(int collectedOrbs, int totalOrbs)
    {
        if (ProgressReportEvent != null) ProgressReportEvent(collectedOrbs, totalOrbs);
    }

    public static event Action VictoryOnUIEvent;
    void VictoryOnUI()
    {
        if (VictoryOnUIEvent != null) VictoryOnUIEvent();
    }

    public static event Action GameOverOnUIEvent;
    void GameOverOnUI()
    {
        if (GameOverOnUIEvent != null) GameOverOnUIEvent();
    }

    public static event Action<int> CountdownEndsEvent;
    void CountdownEnds(int timeRemaining)
    {
        if (CountdownEndsEvent != null) CountdownEndsEvent(timeRemaining);
    }

    public static event Action<float> ProximitySignalEvent;
    void ProximitySignal(float frequency)
    {
        if (ProximitySignalEvent != null) ProximitySignalEvent(frequency);
    }
    //---EVENTS

    GameObject[] collectiblePickups = null;
    int pickupsToCollect;

    void OnPickedUp(PickUpType pickUpType)
    {
        switch (pickUpType)
        {
            case PickUpType.COLLECTIBLE:
                {
                    pickupsToCollect--;
                    ProgressReport(collectiblePickups.Length - pickupsToCollect, collectiblePickups.Length);
                    if (pickupsToCollect == 0) OnVictory();
                    else if (pickupsToCollect == 1)
                    {                      
                        StartCoroutine(SignalOrbProximity());
                    }
                    break;
                }
            case PickUpType.STATE:
                {
                    if (levelState == LevelState.RED)
                    {
                        SwitchLevelState(LevelState.BLUE);
                        StartCoroutine(Countdown());
                    }
                    break;
                }
        }
    }

    IEnumerator Countdown()
    {
        int countdown = blueStateDuration;
        for (int i = blueStateDuration; i > 0; i--)
        {
            countdown--;
            if (countdown < 3 && !victory && !gameOver) CountdownEnds(countdown);
            yield return oneSecond;
        }
        if (!victory && !gameOver) SwitchLevelState(LevelState.RED);
    }

    void OnVictory()
    {
        if (!victory && !gameOver)
        {
            victory = true;
            VictoryOnUI();
            SwitchLevelState(LevelState.BLUE);

            //Save best time
            if (elapsedTime < SaveManager.bestTimes[0] || SaveManager.bestTimes[0] == 0)
            {
                SaveManager.bestTimes[0] = elapsedTime; //TODO handle level number
                SaveManager.SaveData();
            }
        }
    }

    void OnGameOver(GameObject killedPawn)
    {
        if (killedPawn == player && !victory && !gameOver)
        {
            gameOver = true;
            GameOverOnUI();
        }
    }

    IEnumerator SignalOrbProximity()
    {
        yield return new WaitForSeconds(1.5f);
        lastRemainingPickup = GameObject.FindGameObjectWithTag("StandardPickup");
        float frequency = 0f;

        while (!gameOver && !victory)
        {
            float distanceFromPlayer = Vector3.Distance(player.transform.position, lastRemainingPickup.transform.position);
            if (distanceFromPlayer > 20f)
            {
                frequency = 2f;
            }
            else if (distanceFromPlayer <= 20f && distanceFromPlayer > 15f)
            {
                frequency = 1.5f;
            }
            else if (distanceFromPlayer <= 15f && distanceFromPlayer > 10f)
            {
                frequency = 1f;
            }
            else if (distanceFromPlayer <= 10f && distanceFromPlayer > 5f)
            {
                frequency = 0.5f;
            }
            else if (distanceFromPlayer <= 5f)
            {
                frequency = 0.25f;
            }

            ProximitySignal(frequency);
            yield return new WaitForSeconds(frequency);
        }
    }

    IEnumerator OrbTutorial()
    {
        yield return new WaitForSeconds(1.5f);
        TutorialHelper.ShowTutorial(0);
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene == SceneManager.GetSceneByName("PlayerUI"))
        {
            SceneManager.MergeScenes(SceneManager.GetSceneByName("PlayerUI"), SceneManager.GetActiveScene());
            if (pickupsToCollect == 1) StartCoroutine(SignalOrbProximity());
            ProgressReport(collectiblePickups.Length - pickupsToCollect, collectiblePickups.Length);
        }
    }

    void Awake()
    {
        PickUp.OnPickUpEvent += OnPickedUp;
        Health.OnKilledEvent += OnGameOver;
        SceneManager.sceneLoaded += OnSceneLoaded;

        player = GameObject.FindGameObjectWithTag("Player");
    }

    void OnDestroy()
    {
        PickUp.OnPickUpEvent -= OnPickedUp;
        Health.OnKilledEvent -= OnGameOver;
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void Start()
    {
        gameOver = false;
        victory = false;
        Time.timeScale = 1;
        elapsedTime = 0;

        oneSecond = new WaitForSeconds(1);

        collectiblePickups = GameObject.FindGameObjectsWithTag("StandardPickup");
        pickupsToCollect = collectiblePickups.Length;

        SceneManager.LoadScene("PlayerUI", LoadSceneMode.Additive);

        StartCoroutine(OrbTutorial());
    }

    void Update()
    {
        //RESTART
        if (gameOver && Input.GetButtonDown("Restart"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        //CONTINUE
        else if (victory && Input.anyKeyDown)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); //TODO next level
        }
    }
}

