﻿using UnityEngine;
using UnityEngine.Audio;
using System;

//Stores game configuration
public class GameData : SingletonBase<GameData>
{
    [SerializeField]
    AudioMixer masterMixer = null;

    public const string gamepadState = "GamepadState";
    public const string soundVolume = "SoundVolume";
    public const string musicVolume = "MusicVolume";
    public const string currentLanguage = "CurrentLanguage";
    public const string gamepadSensitivity = "GamepadSensitivity";
    public const string mouseSensitivity = "MouseSensitivity";

    void Awake()
    {
        Application.targetFrameRate = 60;

        if (!PlayerPrefs.HasKey(gamepadState)) PlayerPrefs.SetInt(gamepadState, Convert.ToInt32(false));
        if (!PlayerPrefs.HasKey(soundVolume)) PlayerPrefs.SetFloat(soundVolume, 0);
        if (!PlayerPrefs.HasKey(musicVolume)) PlayerPrefs.SetFloat(musicVolume, 0);
        if (!PlayerPrefs.HasKey(currentLanguage)) PlayerPrefs.SetInt(currentLanguage, 0);
        if (!PlayerPrefs.HasKey(gamepadSensitivity)) PlayerPrefs.SetFloat(gamepadSensitivity, 50);
        if (!PlayerPrefs.HasKey(mouseSensitivity)) PlayerPrefs.SetFloat(mouseSensitivity, 50);
    }

    void Start()
    {
        masterMixer.SetFloat("SoundVolume", PlayerPrefs.GetFloat(soundVolume));
        masterMixer.SetFloat("MusicVolume", PlayerPrefs.GetFloat(musicVolume));
    }
}
