﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : SingletonBase<GameManager>
{
    [HideInInspector]
    public int sceneToLoad;

    //TODO: currently experimental and not used
    /*
    public enum GameState
    {
        DEFAULT,
        INITIALIZATION,
        MENU,
        LOADING,
        CORE
    };

    public GameState gameState = GameState.DEFAULT;

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (GameObject.FindGameObjectWithTag("Player"))
        {
            gameState = GameState.CORE;
        }
        else if (GameObject.FindGameObjectWithTag("TitleText"))
        {
            gameState = GameState.MENU;
        }
        else if (GameObject.FindGameObjectWithTag("LoadingText"))
        {
            gameState = GameState.LOADING;
        }
    }
    */

    void Awake ()
    {
        DontDestroyOnLoad(gameObject);
        //gameState = GameState.INITIALIZATION;
        //SceneManager.sceneLoaded += OnSceneLoaded;
    }

    /*
    void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
    */
}
