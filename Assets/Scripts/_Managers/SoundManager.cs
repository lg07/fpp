﻿using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [SerializeField]
    AudioSource stateBlueSound = null;

    [SerializeField]
    AudioSource stateRedSound = null;

    [SerializeField]
    AudioSource standardPickupSound = null;

    [SerializeField]
    AudioSource gameOverSound = null;

    [SerializeField]
    AudioSource victorySound = null;

    [SerializeField]
    AudioSource countdownSound = null;

    void OnPickedUp(LevelController.PickUpType pickUpType)
    {
        switch (pickUpType)
        {
            case LevelController.PickUpType.COLLECTIBLE:
                {
                    standardPickupSound.Play();
                    break;
                }
            case LevelController.PickUpType.STATE:
                {
                    stateBlueSound.Play();
                    break;
                }
        }
    }

    void PlayStateRedSound(LevelController.LevelState state)
    {
        if (state == LevelController.LevelState.RED) stateRedSound.Play();
    }

    void PlayGameOverSound()
    {
        gameOverSound.Play();
    }

    void PlayVictorySound()
    {
        victorySound.Play();
    }

    void PlayCountdownSound(int countdown)
    {
        switch (countdown)
        {
            case 0:
                {
                    countdownSound.volume = 1;
                    break;
                }
            case 1:
                {
                    countdownSound.volume = 0.75f;
                    break;
                }
            case 2:
                {
                    countdownSound.volume = 0.5f;
                    break;
                }
        }
        countdownSound.Play();
    }

    void Awake()
    {
        LevelController.SwitchLevelStateEvent += PlayStateRedSound;
        LevelController.GameOverOnUIEvent += PlayGameOverSound;
        LevelController.VictoryOnUIEvent += PlayVictorySound;
        LevelController.CountdownEndsEvent += PlayCountdownSound;
        PickUp.OnPickUpEvent += OnPickedUp;               
    }

    void OnDestroy()
    {
        LevelController.SwitchLevelStateEvent -= PlayStateRedSound;
        LevelController.GameOverOnUIEvent -= PlayGameOverSound;
        LevelController.VictoryOnUIEvent -= PlayVictorySound;
        LevelController.CountdownEndsEvent -= PlayCountdownSound;
        PickUp.OnPickUpEvent -= OnPickedUp;
    }
}
