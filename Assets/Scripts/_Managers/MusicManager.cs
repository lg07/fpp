﻿using System.Collections;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    [SerializeField]
    AudioSource actionMusic = null;
    [SerializeField]
    AudioSource explorationMusic = null;

    int enemiesAwareOfPlayer = 0;
    WaitForEndOfFrame waitSingleFrame;

    IEnumerator FadeInMusic(AudioSource music)
    {
        while (music.volume < 1)
        {
            music.volume += Time.deltaTime / 50;
            yield return waitSingleFrame;
        }
    }

    IEnumerator FadeOutMusic(AudioSource music)
    {
        while (music.volume > 0)
        {
            music.volume -= Time.deltaTime / 30;
            yield return waitSingleFrame;
        }
    }

    void UpdateAwarenessState(AITargetDetection.AwarenessState state)
    {
        switch (state)
        {
            case AITargetDetection.AwarenessState.ENGAGING:
                {
                    if (enemiesAwareOfPlayer == 0)
                    {
                        StopAllCoroutines();
                        StartCoroutine(FadeInMusic(actionMusic));
                    }
                    enemiesAwareOfPlayer++;
                    break;
                }

            case AITargetDetection.AwarenessState.SEARCHING:
                {
                    if (enemiesAwareOfPlayer > 0) enemiesAwareOfPlayer--;
                    if (enemiesAwareOfPlayer == 0)
                    {
                        StopAllCoroutines();
                        StartCoroutine(FadeOutMusic(actionMusic));
                    }
                    break;
                }

            case AITargetDetection.AwarenessState.UNAWARE:
                {
                    if (enemiesAwareOfPlayer > 0) enemiesAwareOfPlayer--;
                    if (enemiesAwareOfPlayer == 0)
                    {
                        StopAllCoroutines();
                        StartCoroutine(FadeOutMusic(actionMusic));
                    }
                    break;
                }
        }
    }

    void OnVictory()
    {
        actionMusic.Stop();
        explorationMusic.Stop();
    }

    void Awake ()
    {
        AITargetDetection.AwarenessStateChangedEvent += UpdateAwarenessState;
        LevelController.VictoryOnUIEvent += OnVictory;

        waitSingleFrame = new WaitForEndOfFrame();
    }

    void OnDestroy()
    {
        AITargetDetection.AwarenessStateChangedEvent -= UpdateAwarenessState;
        LevelController.VictoryOnUIEvent -= OnVictory;
    }
}
