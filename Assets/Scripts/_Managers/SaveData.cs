﻿using UnityEngine;
using System;

[Serializable]
public struct SaveData
{
    [HideInInspector]
    public int[] bestTimes;
}
