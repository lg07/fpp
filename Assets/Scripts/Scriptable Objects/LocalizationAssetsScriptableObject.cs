﻿using UnityEngine;

[CreateAssetMenu(fileName = "LocalizationAssets", menuName = "Scriptable Objects/LocalizationAssets", order = 2)]
public class LocalizationAssetsScriptableObject : ScriptableObject
{
    public string objectName = "LocalizationAssets";

    public TextAsset[] localizationFiles = null;
}