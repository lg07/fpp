﻿using UnityEngine;

[CreateAssetMenu(fileName = "LevelData", menuName = "Scriptable Objects/LevelData", order = 1)]
public class LevelDataScriptableObject : ScriptableObject
{
    public string objectName = "LevelData";
    [ColorUsage(true, true)]
    public Color[] stateColors;
}
