﻿using UnityEngine;

[CreateAssetMenu(fileName = "Weapons", menuName = "Scriptable Objects/Weapons", order = 3)]
public class WeaponsScriptableObject : ScriptableObject
{
    public string objectName = "Weapons";

    public WeaponItem[] weapons = null;
}