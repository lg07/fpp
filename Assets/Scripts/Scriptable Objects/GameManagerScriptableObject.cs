﻿using UnityEngine;

[CreateAssetMenu(fileName = "GameManager", menuName = "Scriptable Objects/GameManager", order = 0)]
public class GameManagerScriptableObject : ScriptableObject
{
    public string objectName = "GameManager";

    public GameObject HUD;
}
