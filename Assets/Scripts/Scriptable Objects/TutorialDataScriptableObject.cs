﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Tutorials", menuName = "Scriptable Objects/Tutorials", order = 4)]
public class TutorialDataScriptableObject : ScriptableObject
{
    public string objectName = "Tutorials";

    public TutorialWindowData[] tutorials = null;
}
