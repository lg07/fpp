﻿using System;

[Serializable]
public class LocalizationObject
{
    public string key;
    public string text;
}
