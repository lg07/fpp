﻿using System.Collections.Generic;
using System;

[Serializable]
public class LocalizationList
{
    public List<LocalizationObject> localizationObjects;
}
