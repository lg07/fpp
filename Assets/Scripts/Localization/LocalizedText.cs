﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Localization;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.IO;

public class LocalizedText : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Handled automatically, do NOT change manually")]
    bool keyAdded = false; //TODO should be ReadOnly

    [SerializeField]
    LocalizationManager.Category category = LocalizationManager.Category.DEFAULT;

    [SerializeField]
    [Tooltip("Handled automatically, do NOT change manually")]
    string key = ""; //Localization key to get localization from JSON //TODO should be ReadOnly

    Text localizedTextUI;
    TextMesh localizedTextMesh;
    TMP_Text localizedTextMeshPro;

    LocalizationManager localizationManager;

    string baseText; //text in Text component field

#if UNITY_EDITOR

    List<TextAsset> FindLocalizationFiles()
    {
        List<TextAsset> localizationFiles = new List<TextAsset>();

        GameData gameManager = AssetDatabase.LoadAssetAtPath<GameData>("Assets/Resources/GameManager.prefab");
        LocalizationManager localizationManager = gameManager.gameObject.GetComponent<LocalizationManager>();

        foreach (var localizationFile in localizationManager.localizationAssets[(int)category].localizationFiles)
        {
            localizationFiles.Add(localizationFile);
        }

        return localizationFiles;
    }

    char RandomLetter()
    {
        char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
        int num = Random.Range(0, 26);
        char letter = alphabet[num];

        return letter;
    }

    //Generates random sequence of 8 characters, either 1-9 or A-Z
    string GenerateKey()
    {
        string key = "";

        for (int i = 0; i < 8; i++)
        {
            if (i == 0 || i == 3 || i == 6) key += RandomLetter();
            else key += "" + Random.Range(0, 10);
        }

        return key;
    }

    public void CreateNewLocalizationKey()
    {
        AssetDatabase.Refresh();

        if (!keyAdded)
        {
            localizedTextUI = GetComponent<Text>();
            if (localizedTextUI) baseText = localizedTextUI.text;
            else if (!localizedTextUI)
            {
                localizedTextMesh = GetComponent<TextMesh>();
                if (localizedTextMesh) baseText = localizedTextMesh.text;
                else if (!localizedTextMesh)
                {
                    localizedTextMeshPro = GetComponent<TMP_Text>();
                    if (localizedTextMeshPro) baseText = localizedTextMeshPro.text;
                    else if (!localizedTextMeshPro) Debug.Log("Missing expected text component for " + gameObject.name);
                }
            }

            key = GenerateKey();

            foreach (var localizationFile in FindLocalizationFiles())
            {
                LocalizationList localizationList = JsonUtility.FromJson<LocalizationList>(localizationFile.text);

                LocalizationObject newLocalizationObject = new LocalizationObject();
                newLocalizationObject.key = key;
                newLocalizationObject.text = baseText;

                localizationList.localizationObjects.Add(newLocalizationObject);

                string newJsonText = JsonUtility.ToJson(localizationList, true);
                string[] newJsonTexts = new[] { newJsonText };

                File.WriteAllLines(AssetDatabase.GetAssetPath(localizationFile), newJsonTexts);

                Debug.Log("New localization key added to " + localizationFile.name);
            }

            keyAdded = true;
        }

        else //only generate a new key
        {
            string newKey = GenerateKey();

            foreach (var localizationFile in FindLocalizationFiles())
            {
                LocalizationList localizationList = JsonUtility.FromJson<LocalizationList>(localizationFile.text);

                for (int i = 0; i < localizationList.localizationObjects.Count; i++)
                {
                    if (localizationList.localizationObjects[i].key == key)
                    {
                        localizationList.localizationObjects[i].key = newKey;
                        Debug.Log("Key replaced");
                        break;
                    }
                }

                string newJsonText = JsonUtility.ToJson(localizationList, true);
                string[] newJsonTexts = new[] { newJsonText };

                File.WriteAllLines(AssetDatabase.GetAssetPath(localizationFile), newJsonTexts);

                Debug.Log("New localization key generated in " + localizationFile.name);
            }

            key = newKey;
        }
    }

    public void DeleteLocalizationKey()
    {
        if (keyAdded)
        {
            AssetDatabase.Refresh();

            foreach (var localizationFile in FindLocalizationFiles())
            {
                LocalizationList localizationList = JsonUtility.FromJson<LocalizationList>(localizationFile.text);

                for (int i = 0; i < localizationList.localizationObjects.Count; i++)
                {
                    if (localizationList.localizationObjects[i].key == key)
                    {
                        localizationList.localizationObjects.Remove(localizationList.localizationObjects[i]);
                        break;
                    }
                }

                string newJsonText = JsonUtility.ToJson(localizationList, true);
                string[] newJsonTexts = new[] { newJsonText };

                File.WriteAllLines(AssetDatabase.GetAssetPath(localizationFile), newJsonTexts);

                Debug.Log("Localization key removed from " + localizationFile.name);
            }

            key = "";
            keyAdded = false;
        }
    }

#endif

    void GetLocalization()
    {
        if (localizedTextUI) localizedTextUI.text = localizationManager.GetLocalization(key, category);
        else if (localizedTextMesh) localizedTextMesh.text = localizationManager.GetLocalization(key, category);
        else if (localizedTextMeshPro) localizedTextMeshPro.text = localizationManager.GetLocalization(key, category);
    }

    void Awake()
    {
        LocalizationManager.LanguageVersionChangedEvent += GetLocalization;

        localizationManager = LocalizationManager.Instance.GetComponent<LocalizationManager>();
        localizedTextUI = GetComponent<Text>();
        if (!localizedTextUI)
        {
            localizedTextMesh = GetComponent<TextMesh>();
            if (!localizedTextMesh)
            {
                localizedTextMeshPro = GetComponent<TMP_Text>();
                if (!localizedTextMeshPro) Debug.Log("Missing expected text component for " + gameObject.name);
            }
        }

        GetLocalization();
    }

    void OnDestroy()
    {
        LocalizationManager.LanguageVersionChangedEvent -= GetLocalization;
    }
}
