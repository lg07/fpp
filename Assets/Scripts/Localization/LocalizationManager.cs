﻿using UnityEngine;
using System;

//TO ADD A NEW LANGUAGE:
//
//1. Add language code in LanguageVersion enum
//2. Add a new localization JSON file for each Category in /Assets/Localization
//3. Add the new JSON to LocalizationFiles list in each localization Scriptable Object in /Assets/ScriptableObjects/Localization
//NOTE: the language order in the LocalizationFiles list must be the same as in the LanguageVersion enum
//4. Define a new language name in GetLanguage name in LanguageSelection class

//TO ADD A NEW CATEGORY:
//
//1. Add category in Category enum
//2. Create a new folder and add new localization JSON files for the new Category in /Assets/Localization
//3. Add a new ScriptableObject Localization Asset in /Assets/ScriptableObjects/Localization for the new category
//4. Add the new JSON files to LocalizationFiles list in the new Scriptable Object
//NOTE: the language order in the LocalizationFiles list must be the same as in the LanguageVersion enum
//5. Add the LocalizationAssets file to LocalizationAssets list in the LocalizationManager component in Resources/GameData
//NOTE: the categories order in the LocalizationAssets list must be the same as in the Category enum

namespace Localization
{
    public class LocalizationManager : SingletonBase<LocalizationManager>
    {
        public LocalizationAssetsScriptableObject[] localizationAssets;
        LocalizationAssetsScriptableObject localizationAsset;
        LocalizationList localizationList;

        public enum LanguageVersion { EN_US, PL_PL }; //the order must be the same as in the LocalizationAssets scriptable objects
        LanguageVersion languageVersion = LanguageVersion.EN_US;

        public enum Category { DEFAULT, HUD, MENU, OPTIONS, TUTORIALS}; //the order must be the same as in the LocalizationAssets list in the LocalizationManager component in Resources/GameData 

        public static event Action LanguageVersionChangedEvent;
        void LanguageVersionChanged()
        {
            if (LanguageVersionChangedEvent != null) LanguageVersionChangedEvent();
        }

        public void ChangeLanguageVersion(LanguageVersion languageVersion)
        {
            this.languageVersion = languageVersion;
            LanguageVersionChanged();
        }

        void AssignLocalizationAsset(Category category)
        {
            localizationAsset = localizationAssets[(int)category];
        }

        string GetLocalizationForLanguage(string stringKey, LanguageVersion currentLanguageVersion, Category stringCategory)
        {
            localizationList = JsonUtility.FromJson<LocalizationList>(localizationAsset.localizationFiles[(int)currentLanguageVersion].text);

            for (int i = 0; i < localizationList.localizationObjects.Count; i++)
            {
                if (localizationList.localizationObjects[i].key == stringKey)
                {
                    return localizationList.localizationObjects[i].text;
                }
            }

            Debug.Log("[missing localization, key: " + stringKey + ", category: "+ stringCategory + "]");
            return "[missing localization]";
        }

        public string GetLocalization(string key, Category category)
        {
            AssignLocalizationAsset(category);

            if (localizationAsset.localizationFiles.Length > (int)languageVersion)
            {
                return GetLocalizationForLanguage(key, languageVersion, category);
            }

            else
            {
                Debug.Log("[No localization file for: " + languageVersion.ToString() + "]");
                return GetLocalizationForLanguage(key, LanguageVersion.EN_US, category); //fallback to EN_US
            }
        }

        void Awake()
        {
            languageVersion = (LanguageVersion)PlayerPrefs.GetInt(GameData.currentLanguage);
        }
    }
}
