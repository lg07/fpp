﻿using UnityEngine;

//Forces 16:9 aspect ratio, added to scene canvases in AspectRatioEnforcer class

public class ResizeScreen : MonoBehaviour
{

    float GetWindowaspect()
    {
        float windowaspect;

        windowaspect = (float)Screen.width / (float)Screen.height;

        return windowaspect;
    }

    float GetTargetaspect()
    {
        float Targetaspect;

        Targetaspect = 16.0f / 9.0f;

        return Targetaspect;
    }

    Rect GetNewRect(float targetaspect, float windowaspect, Rect CurrentRect)
    {
        float sidescale = windowaspect / targetaspect;
        Rect rect = CurrentRect;


        if (sidescale < 1.0f)
        {
            rect.width = 1.0f;
            rect.height = sidescale;
            rect.x = 0;
            rect.y = (1.0f - sidescale) / 2.0f;
        }
        else
        {
            float scalewidth = 1.0f / sidescale;
            rect.width = scalewidth;
            rect.height = 1.0f;
            rect.x = (1.0f - scalewidth) / 2.0f;
            rect.y = 0;
        }

        return rect;
    }

    void RefreshRectOfCamera(Camera camera)
    {
        float targetaspect = GetTargetaspect();
        float windowaspect = GetWindowaspect();

        camera.rect = GetNewRect(targetaspect, windowaspect, camera.rect);
    }

    void Start()
    {
        RefreshRectOfCamera(GetComponent<Camera>());
    }
}