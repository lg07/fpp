﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//Makes game screen aspect ratio always 16:9 and adds black frames, if neccessary

public class AspectRatioEnforcer : MonoBehaviour
{
    [SerializeField]
    GameObject blackCamera = null;

    void Awake()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Canvas[] canvases = FindObjectsOfType<Canvas>();
        Camera[] cameras = FindObjectsOfType<Camera>();

        foreach (var camera in cameras)
        {
            if (!camera.GetComponent<ResizeScreen>())
            {
                camera.gameObject.AddComponent<ResizeScreen>();
                GameObject blackCameraObject = Instantiate(blackCamera, camera.gameObject.transform) as GameObject;
            }
        }

        foreach (var canvas in canvases)
        {
            if (!canvas.GetComponent<AspectRatioFitter>())
            {
                canvas.gameObject.AddComponent<AspectRatioFitter>();
                canvas.gameObject.GetComponent<AspectRatioFitter>().aspectMode = AspectRatioFitter.AspectMode.WidthControlsHeight;
                canvas.gameObject.GetComponent<AspectRatioFitter>().aspectRatio = 1.777975f;
            }
        }
    }
}
