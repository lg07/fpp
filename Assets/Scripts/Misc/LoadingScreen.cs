﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadingScreen : MonoBehaviour
{
    private AsyncOperation async;
    private int sceneToLoad;

    //load level
    IEnumerator LoadingNextLevel(int level)
    {
        yield return new WaitForSeconds(0.5f);
        async = SceneManager.LoadSceneAsync(level);
        while (!async.isDone)
        {
            yield return null;
        }
    }

    void Start()
    {
        Time.timeScale = 1.0f; //to deactivate Pause, if needed
        sceneToLoad = GameManager.Instance.sceneToLoad;
        StartCoroutine(LoadingNextLevel(sceneToLoad));
    }
}
