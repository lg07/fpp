﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class SensitivitySettings : MonoBehaviour
{
    [SerializeField]
    Slider gamepadSensitivitySlider = null;
    [SerializeField]
    Slider mouseSensitivitySlider = null;

    public static event Action SensitivitySettingsChangedEvent;
    void SensitivitySettingsChanged()
    {
        if (SensitivitySettingsChangedEvent != null) SensitivitySettingsChangedEvent();
    }

    public void SetGamepadSensitivity(float gamepadSensitivity)
    {
        InputManager.Instance.gamepadSensitivity = gamepadSensitivity;
        SensitivitySettingsChanged();
    }

    public void SetMouseSensitivity(float mouseSensitivity)
    {
        InputManager.Instance.mouseSensitivity = mouseSensitivity;
        SensitivitySettingsChanged();
    }

    public void SaveSensitivitySettings ()
    {
        PlayerPrefs.SetFloat(GameData.gamepadSensitivity, gamepadSensitivitySlider.value);
        PlayerPrefs.SetFloat(GameData.mouseSensitivity, mouseSensitivitySlider.value);
    }
	
	void OnEnable ()
    {
        gamepadSensitivitySlider.value = PlayerPrefs.GetFloat(GameData.gamepadSensitivity);
        mouseSensitivitySlider.value = PlayerPrefs.GetFloat(GameData.mouseSensitivity);
    }

    void ResetToDefaults()
    {
        SetGamepadSensitivity(50);
        gamepadSensitivitySlider.value = 50;
        SetMouseSensitivity(50);
        mouseSensitivitySlider.value = 50;
    }

    void Awake()
    {
        OptionsPanel.ResetToDefaultsEvent += ResetToDefaults;
    }
    void OnDestroy()
    {
        OptionsPanel.ResetToDefaultsEvent -= ResetToDefaults;
    }
}
