﻿using UnityEngine;
using UnityEngine.EventSystems;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    GameObject tutorialsPanel = null;
    [SerializeField]
    GameObject optionsPanel = null;
    [SerializeField]
    GameObject controlsPanel = null;

    void OnGamepadStateChanged(bool gamepadOn)
    {
        if (gamepadOn)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            if (!Application.isEditor) Cursor.lockState = CursorLockMode.Confined;
            EventSystem.current.SetSelectedGameObject(null);
        }
    }

    void Awake()
    {
        InputManager.GamepadStateChangedEvent += OnGamepadStateChanged;
        optionsPanel.SetActive(false);
        tutorialsPanel.SetActive(false);
        controlsPanel.SetActive(false);
    }

    void OnDestroy()
    {
        InputManager.GamepadStateChangedEvent -= OnGamepadStateChanged;
    }
}
