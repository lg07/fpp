﻿using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class MainMenuButtons : UIPanel
{
    [SerializeField]
    Button[] menuButtons = null;

    public void OnClickStartButton()
    {
        GameManager.Instance.sceneToLoad = 2;
        SceneManager.LoadScene(1);
    }

    public void OnClickExitButton()
    {
        PlayerPrefs.SetInt(GameData.gamepadState, Convert.ToInt32(InputManager.Instance.GamepadOn));
        Application.Quit();
    }

    protected override void OnGamepadStateChanged(bool state)
    {
        if (state)
        {
            menuButtons[0].Select();
        }
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        OnGamepadStateChanged(InputManager.Instance.GamepadOn);
    }
}
