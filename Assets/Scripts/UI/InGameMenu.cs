﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class InGameMenu : MonoBehaviour
{
    [SerializeField]
    UIPanel menuButtons = null;
    [SerializeField]
    GameObject optionsPanel = null;
    [SerializeField]
    GameObject tutorialsPanel = null;
    [SerializeField]
    GameObject controlsPanel = null;

    public void ExitToMainMenu()
    {
        InputManager.Instance.SetPlayerInput(true);
        Time.timeScale = 1;

        GameManager.Instance.sceneToLoad = 0;
        SceneManager.LoadScene(1);
    }

    public void ResumeGame()
    {
        menuButtons.OnClosed();
        menuButtons.gameObject.SetActive(false);
    }

    void ActivateInGameMenu()
    {
        menuButtons.gameObject.SetActive(true);
        menuButtons.OnOpened();
        OnGamepadStateChanged(InputManager.Instance.GamepadOn);
    }

    void OnGamepadStateChanged(bool gamepadOn)
    {
        if (menuButtons.gameObject.activeInHierarchy || optionsPanel.activeInHierarchy || tutorialsPanel.activeInHierarchy)
        {
            if (gamepadOn)
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
            else
            {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                if (!Application.isEditor) Cursor.lockState = CursorLockMode.Confined;
                EventSystem.current.SetSelectedGameObject(null);
            }
        }
    }

    void Awake()
    {
        InputManager.GamepadStateChangedEvent += OnGamepadStateChanged;
    }

    void OnDestroy()
    {
        InputManager.GamepadStateChangedEvent -= OnGamepadStateChanged;
    }

    void Start ()
    {
        menuButtons.gameObject.SetActive(false);
        optionsPanel.SetActive(false);
        tutorialsPanel.SetActive(false);
        controlsPanel.SetActive(false);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Joystick1Button6) && !LevelController.Instance.Victory)
        {
            if (!menuButtons.gameObject.activeInHierarchy && !optionsPanel.activeInHierarchy && !tutorialsPanel.activeInHierarchy)
            {
                ActivateInGameMenu();
            }
            else if (menuButtons.gameObject.activeInHierarchy)
            {
                ResumeGame();
            }
        }
    }
}
