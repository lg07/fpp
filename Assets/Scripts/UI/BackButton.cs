﻿using UnityEngine;
using UnityEngine.UI;

public class BackButton : MonoBehaviour
{
    Button button = null;
    [SerializeField]
    Image xBoxButtonImage = null;
    [SerializeField]
    [Tooltip("Name of the game object this button should activate")]
    string activatedObject = ""; //perhaps there is some better solution...

    GameObject lowerUIObject = null;

    public void OnClickBackButton()
    {
        if (lowerUIObject) lowerUIObject.SetActive(true);

        if (transform.parent.gameObject.GetComponent<UIPanel>())
        {
            transform.parent.gameObject.GetComponent<UIPanel>().OnClosed();
        }

        transform.parent.gameObject.SetActive(false);
    }

    void OnGamepadStateChanged(bool state)
    {
        if (state)
        {
            button.interactable = false;
            xBoxButtonImage.enabled = true;
        }
        else
        {
            button.interactable = true;
            xBoxButtonImage.enabled = false;
        }
    }

    void Awake()
    {
        button = GetComponent<Button>();
        lowerUIObject = GameObject.Find(activatedObject);
    }

    void OnEnable()
    {
        InputManager.GamepadStateChangedEvent += OnGamepadStateChanged;
        OnGamepadStateChanged(InputManager.Instance.GamepadOn);
    }

    void OnDisable()
    {
        InputManager.GamepadStateChangedEvent -= OnGamepadStateChanged;
    }

    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            button.onClick.Invoke();
        }
    }
}
