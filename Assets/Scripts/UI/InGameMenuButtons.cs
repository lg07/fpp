﻿using UnityEngine;
using UnityEngine.UI;

public class InGameMenuButtons : UIPanel
{
    [SerializeField]
    Button[] menuButtons = null;

    protected override void OnGamepadStateChanged(bool state)
    {
        if (state)
        {
            menuButtons[0].Select();
        }
    }
}
