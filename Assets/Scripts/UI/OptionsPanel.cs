﻿using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class OptionsPanel : UIPanel
{
    [SerializeField]
    Dropdown languageDropdown = null;
    [SerializeField]
    Slider soundVolumeSlider = null;
    [SerializeField]
    Slider gamepadSensitivitySlider = null;
    [SerializeField]
    Slider mouseSensitivitySlider = null;

    protected override void OnGamepadStateChanged(bool state)
    {
        if (state)
        {
            if (languageDropdown) languageDropdown.Select();
            else if (soundVolumeSlider) soundVolumeSlider.Select();

            gamepadSensitivitySlider.gameObject.SetActive(true);
            mouseSensitivitySlider.gameObject.SetActive(false);
        }
        else
        {           
            gamepadSensitivitySlider.gameObject.SetActive(false);
            mouseSensitivitySlider.gameObject.SetActive(true);
        }
    }

    public static event Action ResetToDefaultsEvent;
    public void ResetToDefaults()
    {
        if (ResetToDefaultsEvent != null) ResetToDefaultsEvent();
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        OnGamepadStateChanged(InputManager.Instance.GamepadOn);
        if (SceneManager.GetActiveScene().buildIndex != 0)
        {
            languageDropdown.gameObject.SetActive(false);
            soundVolumeSlider.Select();
        }
    }
}
