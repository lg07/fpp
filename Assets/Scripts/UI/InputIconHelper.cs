﻿using UnityEngine;
using TMPro;

//Changes Text Mesh Pro sprite reference Asset
//when switching between mouse & keyboard and gamepad - to display proper input icons.

public class InputIconHelper : MonoBehaviour
{
    [SerializeField]
    TMP_SpriteAsset gamepadAsset = null;
    [SerializeField]
    TMP_SpriteAsset keyboardAsset = null;

    TMP_Text text = null;

    void OnGamepadStateChanged(bool state)
    {
        if (state)
        {
            if (text) text.spriteAsset = gamepadAsset;
        }
        else
        {
            if (text) text.spriteAsset = keyboardAsset;
        }
    }

    void Awake()
    {
        text = GetComponent<TMP_Text>();
    }

    void OnEnable()
    {
        InputManager.GamepadStateChangedEvent += OnGamepadStateChanged;
        OnGamepadStateChanged(InputManager.Instance.GamepadOn);
    }

    void OnDisable()
    {
        InputManager.GamepadStateChangedEvent -= OnGamepadStateChanged;
    }
}
