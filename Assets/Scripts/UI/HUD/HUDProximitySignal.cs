﻿using System.Collections;
using UnityEngine;

public class HUDProximitySignal : MonoBehaviour
{
    [SerializeField]
    GameObject signalCircle = null;

    AudioSource signalSFX = null;

    IEnumerator DisplaySingleSignal(float frequency)
    {
        signalCircle.SetActive(false);
        signalCircle.SetActive(true);

        if (frequency > 1.5f)
        {
            signalSFX.pitch = 0.8f;
        }
        else if (frequency <= 1.5f && frequency > 1f)
        {
            signalSFX.pitch = 0.9f;
        }
        else if (frequency <= 1f && frequency > 0.5f)
        {
            signalSFX.pitch = 1f;
        }
        else if (frequency <= 0.5f && frequency > 0.25f)
        {
            signalSFX.pitch = 1.1f;
        }
        else if (frequency <= 0.25f)
        {
            signalSFX.pitch = 1.2f;
        }

        signalSFX.Play();

        yield return new WaitForSeconds(0.5f);
        signalCircle.SetActive(false);
    }

    void DisplaySignal(float frequency)
    {
        StopAllCoroutines();
        StartCoroutine(DisplaySingleSignal(frequency));
    }

    void Awake()
    {
        LevelController.ProximitySignalEvent += DisplaySignal;
        signalSFX = GetComponent<AudioSource>();
    }

    void OnDestroy()
    {
        LevelController.ProximitySignalEvent -= DisplaySignal;
    }

    void Start ()
    {
        signalCircle.SetActive(false);
    }
}
