﻿using UnityEngine;
using UnityEngine.UI;

public class HUDLevelProgress : MonoBehaviour
{
    [SerializeField]
    Text dataOrbsText = null;
    [SerializeField]
    Text dataOrbsFullText = null;

    void UpdateProgress(int collectedOrbs, int totalOrbs)
    {
        dataOrbsFullText.text = dataOrbsText.text + ": " + collectedOrbs + "/" + totalOrbs;
    }

    void Awake()
    {
        LevelController.ProgressReportEvent += UpdateProgress;
    }

    void OnDestroy()
    {
        LevelController.ProgressReportEvent -= UpdateProgress;
    }
}
