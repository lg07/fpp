﻿using UnityEngine;
using System;

[Serializable]
public class TutorialWindowData : object
{
    public string tutorialTitle;
    public string tutorialText;
    public Sprite tutorialSprite;
}
