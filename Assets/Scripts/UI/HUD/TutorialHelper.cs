﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using Localization;

public class TutorialHelper : MonoBehaviour
{
    [SerializeField]
    TutorialDataScriptableObject tutorialData = null;
    [SerializeField]
    UIPanel tutorialWindow = null;
    [SerializeField]
    TMP_Text tutorialTitleText = null;
    [SerializeField]
    TMP_Text tutorialText = null;
    [SerializeField]
    Image tutorialImage = null;

    static Action<int> ShowTutorialWindowEvent;
    public static void ShowTutorial(int id)
    {
        if (ShowTutorialWindowEvent != null) ShowTutorialWindowEvent(id);
    }

    public void ShowTutorialWindow(int id)
    {
        tutorialTitleText.text = LocalizationManager.Instance.GetLocalization(tutorialData.tutorials[id].tutorialTitle, LocalizationManager.Category.TUTORIALS);
        tutorialText.text = LocalizationManager.Instance.GetLocalization(tutorialData.tutorials[id].tutorialText, LocalizationManager.Category.TUTORIALS);
        tutorialImage.sprite = tutorialData.tutorials[id].tutorialSprite;
        tutorialWindow.gameObject.SetActive(true);
        tutorialWindow.OnOpened();
    }

    void Awake()
    {
        ShowTutorialWindowEvent += ShowTutorialWindow;
    }

    void OnDestroy()
    {
        ShowTutorialWindowEvent -= ShowTutorialWindow;
    }

    void Start()
    {
        tutorialWindow.gameObject.SetActive(false);
    }

    void Update()
    {
        if (tutorialWindow.gameObject.activeInHierarchy && (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Joystick1Button6)))
        {
            tutorialWindow.OnClosed();
            tutorialWindow.gameObject.SetActive(false);
        }
    }
}
