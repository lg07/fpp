﻿using UnityEngine;

public class HUDTutorialWindow : UIPanel
{
    protected override void OnGamepadStateChanged(bool gamepadOn)
    {
        if (gamepadOn)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            if (!Application.isEditor) Cursor.lockState = CursorLockMode.Confined;
        }
    }
}
