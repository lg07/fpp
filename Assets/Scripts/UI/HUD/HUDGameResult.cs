﻿using UnityEngine;

public class HUDGameResult : MonoBehaviour
{
    [SerializeField]
    GameObject victoryText = null;
    [SerializeField]
    GameObject gameOverText = null;

    void Victory()
    {
        victoryText.SetActive(true);
    }

    void GameOver()
    {
        gameOverText.SetActive(true);
    }

    void Awake()
    {
        LevelController.VictoryOnUIEvent += Victory;
        LevelController.GameOverOnUIEvent += GameOver;
    }

    void OnDestroy()
    {
        LevelController.VictoryOnUIEvent -= Victory;
        LevelController.GameOverOnUIEvent -= GameOver;
    }

    void Start()
    {
        victoryText.SetActive(false);
        gameOverText.SetActive(false);
    }
}
