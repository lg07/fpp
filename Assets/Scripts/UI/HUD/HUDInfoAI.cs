﻿using UnityEngine;
using UnityEngine.UI;

public class HUDInfoAI : MonoBehaviour
{
    [SerializeField]
    GameObject healthBarAI = null;

    [SerializeField]
    GameObject hackedBarAI = null;

    Slider healthBarAISlider = null;
    RectTransform crosshairsRectTransform;
    GameObject playerCharacter = null;
    IFF playerCharacterIFFComponent = null;
    GameObject playerCameraObject = null;
    Camera playerCamera = null;
    Image[] crosshairImages = null; //TODO single image
    Slider hackedBarAISlider = null;
    Text hackedBarAIText = null;

    void UpdateHackingProgressBar(float hackingProgress)
    {
        hackedBarAISlider.value = hackingProgress;
    }


    void HandleAimingAtEnemy(RaycastHit hit, Ray ray)
    {
        healthBarAI.SetActive(true);
        healthBarAISlider.value = hit.transform.GetComponent<AIHealth>().Health;
        foreach (var image in crosshairImages)
        {
            image.color = Color.red;
        }

        if (!hackedBarAI.activeInHierarchy)
        {
            hackedBarAI.SetActive(true);
            hackedBarAIText.enabled = false;
        }
        if (hackedBarAISlider.value > 1 && !hackedBarAIText.enabled) hackedBarAIText.enabled = true;
        else if (hackedBarAISlider.value < 1 && hackedBarAIText.enabled) hackedBarAIText.enabled = false;
    }

    void HandleAimingAtFriendly(RaycastHit hit, Ray ray)
    {
        healthBarAI.SetActive(true);
        healthBarAISlider.value = hit.transform.GetComponent<AIHealth>().Health;
        foreach (var image in crosshairImages)
        {
            image.color = Color.green;
        }

        if (!hackedBarAI.activeInHierarchy)
        {
            hackedBarAI.SetActive(true);
        }

        if (hackedBarAIText.enabled)
            hackedBarAIText.enabled = false;


        if (hit.transform.GetComponent<IFF>().Hacked)
        {
            hackedBarAISlider.value = (hit.transform.GetComponent<IFF>().RemainingHackedTime / hit.transform.GetComponent<IFF>().HackedTime) * 100;
        }
    }

    void ClearTargetInfo()
    {
        healthBarAI.SetActive(false);
        foreach (var image in crosshairImages)
        {
            image.color = Color.white;
        }

        if (hackedBarAI.activeInHierarchy)
        {
            hackedBarAISlider.value = 0;
            hackedBarAI.SetActive(false);
        }
    }

    void Awake()
    {
        PlayerWeapon.UpdateHackingProgressOnHUDEvent += UpdateHackingProgressBar;

        healthBarAI.SetActive(false);
        crosshairsRectTransform = (RectTransform)transform;
        healthBarAISlider = healthBarAI.GetComponent<Slider>();
        hackedBarAISlider = hackedBarAI.GetComponent<Slider>();
        hackedBarAIText = hackedBarAI.GetComponentInChildren<Text>();
        crosshairImages = GetComponentsInChildren<Image>();
        playerCharacter = GameObject.FindGameObjectWithTag("Player");
        playerCharacterIFFComponent = playerCharacter.GetComponent<IFF>();
    }

    void Start()
    {
        playerCameraObject = GameObject.FindGameObjectWithTag("MainCamera");
        if (playerCameraObject) playerCamera = playerCameraObject.GetComponent<Camera>();
        foreach (var image in crosshairImages)
        {
            image.color = Color.white;
        }

        hackedBarAI.SetActive(false);
    }

    void OnDestroy()
    {
        PlayerWeapon.UpdateHackingProgressOnHUDEvent -= UpdateHackingProgressBar;
    }

    void FixedUpdate()
    {
        RaycastHit hit;
        Ray ray = playerCamera.ScreenPointToRay(crosshairsRectTransform.position);

        if (Physics.Raycast(ray, out hit) && playerCharacterIFFComponent.IsValidTarget(hit.transform.gameObject)
            && playerCharacterIFFComponent.IsTargetHostile(hit.transform.gameObject))
        {
            HandleAimingAtEnemy(hit, ray);
        }

        else if (Physics.Raycast(ray, out hit) && playerCharacterIFFComponent.IsValidTarget(hit.transform.gameObject)
            && !playerCharacterIFFComponent.IsTargetHostile(hit.transform.gameObject))
        {
            HandleAimingAtFriendly(hit, ray);
        }

        else if (healthBarAI.activeInHierarchy)
        {
            ClearTargetInfo();
        }
    }
}
