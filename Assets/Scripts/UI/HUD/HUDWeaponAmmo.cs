﻿using UnityEngine;
using UnityEngine.UI;
using Localization;

public class HUDWeaponAmmo : MonoBehaviour
{
    [SerializeField]
    Text weaponAmmoText = null;

    void OnAmmoUpdated(string weaponAmmoLabel, string currentAmmo)
    {
        weaponAmmoText.text = LocalizationManager.Instance.GetLocalization(weaponAmmoLabel, LocalizationManager.Category.HUD) + ": " + currentAmmo;
    }

    void Awake()
    {
        PlayerWeapon.UpdateAmmoEvent += OnAmmoUpdated;
    }

    void OnDestroy()
    {
        PlayerWeapon.UpdateAmmoEvent -= OnAmmoUpdated;
    }
}
