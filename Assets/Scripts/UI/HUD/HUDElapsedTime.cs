﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class HUDElapsedTime : MonoBehaviour
{
    Text elapsedTimeText = null;
    WaitForSeconds oneSecond = null;
    int counter;

    public static string DisplayCounterText(int counter)
    {
        int minutes = Mathf.FloorToInt(counter / 60);
        int seconds = counter - (minutes * 60);
        int hours = Mathf.FloorToInt(minutes / 60);

        string minutesText;
        string secondsText;
        string hoursText;

        if (minutes < 10) minutesText = "0" + minutes + ":";
        else minutesText = "" + minutes + ":";

        if (seconds < 10) secondsText = "0" + seconds;
        else secondsText = "" + seconds;

        if (hours == 0) hoursText = "";
        else if (hours < 10) hoursText = "0" + hours + ":";
        else hoursText = "" + hours + ":";

        return hoursText + minutesText + secondsText;
    }

    IEnumerator ElapsedTimeCounter()
    {
        while (true)
        {
            yield return oneSecond;
            if (LevelController.Instance.GameOver || LevelController.Instance.Victory) break;

            counter++;
            LevelController.Instance.elapsedTime = counter;

            elapsedTimeText.text = DisplayCounterText(counter);
        }
    }

    void Awake()
    {
        elapsedTimeText = GetComponent<Text>();
    }

    void Start()
    {
        counter = 0;
        oneSecond = new WaitForSeconds(1);
        StartCoroutine(ElapsedTimeCounter());
    }
}
