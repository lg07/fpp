﻿using UnityEngine;
using UnityEngine.UI;

public class HUDHealth : MonoBehaviour
{
    [SerializeField]
    Slider healthBar = null;
    [SerializeField]
    Animator hitEffect = null;

    float currentHealth;
    float healthBarChangeRatio = 75f;

    void PlayerHit (float health)
    {
        currentHealth = health;
        hitEffect.SetTrigger("PlayerHit");
    }

    void Awake()
    {
        PlayerHealth.PlayerHitEvent += PlayerHit;
    }

    void OnDestroy()
    {
        PlayerHealth.PlayerHitEvent -= PlayerHit;
    }

    void Start ()
    {
        currentHealth = 100;
        healthBar.value = currentHealth;
    }

    void Update()
    {
        if (healthBar.value > currentHealth)
        {
            healthBar.value -= Time.deltaTime * healthBarChangeRatio;
            Mathf.Clamp(healthBar.value, 0f, 100f);
        }
    }
}
