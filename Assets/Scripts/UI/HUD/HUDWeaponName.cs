﻿using UnityEngine;
using UnityEngine.UI;
using Localization;

public class HUDWeaponName : MonoBehaviour
{
    [SerializeField]
    Text weaponNameText = null;

    void OnWeaponChanged(string weaponName)
    {
        weaponNameText.text = LocalizationManager.Instance.GetLocalization(weaponName, LocalizationManager.Category.HUD);
    }
    
    void Awake()
    {
        PlayerWeapon.WeaponChangedEvent += OnWeaponChanged;
    }

    void OnDestroy()
    {
        PlayerWeapon.WeaponChangedEvent -= OnWeaponChanged;
    }
}
