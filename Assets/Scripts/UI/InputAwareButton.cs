﻿using UnityEngine;
using UnityEngine.UI;

//Handles mouse&keyboard / gamepad switching base logic for buttons

public class InputAwareButton : MonoBehaviour
{
    [SerializeField]
    Sprite gamepadSprite = null;

    [SerializeField]
    Sprite clickableSprite = null;

    [SerializeField]
    Text clickableButtonText = null;

    [SerializeField]
    KeyCode gamepadInput = KeyCode.Joystick1Button0;

    Button button = null;

    void OnGamepadStateChanged(bool state)
    {
        if (state)
        {
            GetComponent<Image>().sprite = gamepadSprite;
            clickableButtonText.enabled = false;
        }
        else
        {
            GetComponent<Image>().sprite = clickableSprite;
            clickableButtonText.enabled = true;
        }
    }

    void OnEnable()
    {
        InputManager.GamepadStateChangedEvent += OnGamepadStateChanged;
        OnGamepadStateChanged(InputManager.Instance.GamepadOn);
    }

    void OnDisable()
    {
        InputManager.GamepadStateChangedEvent -= OnGamepadStateChanged;
    }

    void Awake ()
    {
        button = GetComponent<Button>();
    }
	
	void Update ()
    {
		if (Input.GetKeyDown(gamepadInput) && button.interactable)
        {
            button.onClick.Invoke();
        }
	}
}
