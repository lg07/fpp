﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TutorialsPanel : MonoBehaviour
{
    [SerializeField]
    Sprite[] tutorialImages = null;
    [SerializeField]
    TMP_Text[] tutorialTexts = null;
    [SerializeField]
    Image tutorialImage = null;
    [SerializeField]
    Button nextButton = null;
    [SerializeField]
    Button previousButton = null;

    int currentPageIndex = 0;

    public void ChangePage(bool nextPage)
    {
        if (nextPage && currentPageIndex < tutorialTexts.Length-1)
        {
            tutorialTexts[currentPageIndex].enabled = false;

            currentPageIndex++;

            tutorialTexts[currentPageIndex].enabled = true;
            tutorialImage.sprite = tutorialImages[currentPageIndex];

            previousButton.interactable = true;
            if (currentPageIndex == tutorialTexts.Length - 1)
            {
                nextButton.interactable = false;
            }
        }
        else if (!nextPage && currentPageIndex > 0)
        {
            tutorialTexts[currentPageIndex].enabled = false;

            currentPageIndex--;

            tutorialTexts[currentPageIndex].enabled = true;
            tutorialImage.sprite = tutorialImages[currentPageIndex];

            nextButton.interactable = true;
            if (currentPageIndex == 0)
            {
                previousButton.interactable = false;
            }
        }
    }

    void OnEnable()
    {
        previousButton.interactable = false;

        currentPageIndex = 0;
        tutorialImage.sprite = tutorialImages[0];
        tutorialTexts[0].enabled = true;

        for (int i=1; i<tutorialTexts.Length; i++)
        {
            tutorialTexts[i].enabled = false;
        }

        nextButton.interactable = true;
    }
}
