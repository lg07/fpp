﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioVolume : MonoBehaviour
{
    [SerializeField]
    AudioMixer masterMixer = null;
    [SerializeField]
    Slider soundSlider = null;
    [SerializeField]
    Slider musicSlider = null;

    public void SetSoundVolume(float soundVol)
    {
        masterMixer.SetFloat("SoundVolume", soundVol);
    }

    public void SetMusicVolume(float musicVol)
    {
        masterMixer.SetFloat("MusicVolume", musicVol);
    }

    public void SaveVolumeChanges()
    {
        PlayerPrefs.SetFloat(GameData.soundVolume, soundSlider.value);
        PlayerPrefs.SetFloat(GameData.musicVolume, musicSlider.value);
    }

    void OnEnable()
    {
        soundSlider.value = PlayerPrefs.GetFloat(GameData.soundVolume);
        musicSlider.value = PlayerPrefs.GetFloat(GameData.musicVolume);
    }

    void ResetToDefaults()
    {
        SetSoundVolume(0);
        soundSlider.value = 0;
        SetMusicVolume(0);
        musicSlider.value = 0;
    }

    void Awake()
    {
        OptionsPanel.ResetToDefaultsEvent += ResetToDefaults;
    }
    void OnDestroy()
    {
        OptionsPanel.ResetToDefaultsEvent -= ResetToDefaults;
    }
}