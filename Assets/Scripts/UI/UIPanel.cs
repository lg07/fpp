﻿using UnityEngine;

//base class for UI panels

public abstract class UIPanel : MonoBehaviour
{
    public bool pauseGame = false;

    protected abstract void OnGamepadStateChanged(bool state);

    void ActivatePlayerInput()
    {
        InputManager.Instance.SetPlayerInput(true);
    }

    public virtual void OnOpened()
    {
        if (pauseGame)
        {
            Time.timeScale = 0;
            InputManager.Instance.SetPlayerInput(false);
        }
    }

    public virtual void OnClosed()
    {
        if (pauseGame)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            Time.timeScale = 1;

            if (LevelController.Instance && !LevelController.Instance.GameOver)
                Invoke("ActivatePlayerInput", 0.2f); //to fix player jumping after selecting button with gamepad A
        }
    }

    protected virtual void OnEnable()
    {
        InputManager.GamepadStateChangedEvent += OnGamepadStateChanged;
        OnGamepadStateChanged(InputManager.Instance.GamepadOn);
    }

    protected virtual void OnDisable()
    {
        InputManager.GamepadStateChangedEvent -= OnGamepadStateChanged;
    }
}
