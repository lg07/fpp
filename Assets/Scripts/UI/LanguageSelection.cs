﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using Localization;

public class LanguageSelection : MonoBehaviour
{
    [SerializeField]
    Dropdown languageDropdown = null;

    LocalizationManager localizationManager;

    public void ChangeLanguage()
    {
        localizationManager.ChangeLanguageVersion((LocalizationManager.LanguageVersion)languageDropdown.value);
        AddLanguagesToList();
    }

    public void SaveLanguageSelection()
    {
        PlayerPrefs.SetInt(GameData.currentLanguage, languageDropdown.value);
    }

    string GetLanguageName(string languageCode)
    {
        switch (languageCode)
        {
            case "EN_US":
                {
                    return "English (US)";
                }
            case "PL_PL":
                {
                    return "Polish";
                }
            default:
                {
                    Debug.Log("Missing language name for " + languageCode);
                    return languageCode;
                }
        }
    }

    void AddLanguagesToList()
    {
        List<string> languagesList = new List<string>();
        Array languagesArray = Enum.GetValues(typeof(LocalizationManager.LanguageVersion));

        foreach (var languagesArrayElement in languagesArray)
        {
            languagesList.Add(GetLanguageName(languagesArrayElement.ToString()));
        }

        languageDropdown.ClearOptions();
        languageDropdown.AddOptions(languagesList);
    }

    void Awake()
    {
        localizationManager = LocalizationManager.Instance.GetComponent<LocalizationManager>();
    }

    void Start()
    {
        languageDropdown.value = PlayerPrefs.GetInt(GameData.currentLanguage);
        AddLanguagesToList();
    }
}
