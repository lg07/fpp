﻿using UnityEngine;
using UnityEngine.UI;

public class BestTime : MonoBehaviour
{
    Text bestTimeText = null;

    void Awake()
    {
        bestTimeText = GetComponent<Text>();
    }

    void OnEnable()
    {
        //TODO handle level number
        if (SaveManager.bestTimes[0] > 0)
            bestTimeText.text = HUDElapsedTime.DisplayCounterText(SaveManager.bestTimes[0]);
        else
            bestTimeText.text = "--:--";
    }
}
