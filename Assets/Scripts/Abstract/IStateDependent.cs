﻿//Interface for all classes that depend on current level state

public interface IStateDependent
{
    void ChangeState(LevelController.LevelState state);
    void InitializeState();
}
