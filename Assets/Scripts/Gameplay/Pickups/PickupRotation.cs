﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupRotation : MonoBehaviour
{
    [SerializeField]
    float speed = 0;

	void Update ()
    {
        transform.RotateAround(transform.position, transform.up, Time.deltaTime * speed);
	}
}
