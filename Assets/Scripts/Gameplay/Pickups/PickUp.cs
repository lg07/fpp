﻿using System;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    [SerializeField]
    LevelController.PickUpType pickUpType = LevelController.PickUpType.COLLECTIBLE;

    protected bool pickedUp;

    public static event Action<LevelController.PickUpType> OnPickUpEvent;
    void OnPickUp()
    {
        if (OnPickUpEvent != null) OnPickUpEvent(pickUpType);
    }

    void ExecutePickUp()
    {
        pickedUp = true;
        OnPickUp();
        Destroy(gameObject);
    }

    void Start()
    {
        pickedUp = false;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !pickedUp)
        {
            ExecutePickUp();          
        }
    }

}
