﻿using UnityEngine;
using System;

//Weapon template class

[Serializable]
public class WeaponItem : MonoBehaviour, IStateDependent
{
    public enum WeaponType { COMBAT, HACKING };

    [Header("General")]
    [Tooltip("Localization Key for weapon name")]
    public string weaponName;
    public WeaponType weaponType = WeaponType.COMBAT;

    [Header("Visuals")]
    public Texture[] weaponTextures = null;

    [Header("Parameters")]
    [SerializeField]
    float damage = 0;
    public float Damage { get { return damage; } }
    [SerializeField]
    float fireRate = 0;
    [SerializeField]
    int clipSize = 0;
    public int ClipSize { get { return clipSize; } }
    int currentAmmo;
    public int CurrentAmmo { get { return currentAmmo; } set { currentAmmo = value; } }
    [SerializeField]
    [Tooltip("Localization Key for ammo label displayed on HUD")]
    string ammoLabel = "Ammo";
    public string AmmoLabel { get { return ammoLabel; } }

    public float FireRate { get { return fireRate; } }
    public Transform gunBarrelEnd = null;

    [Header("FX")]
    public GameObject shotFX = null;
    public Transform shotFXTransform = null;
    public GameObject shotHitFXBlue = null;
    public GameObject shotHitFXRed = null;
    public GameObject weaponSoundObject = null;
    public GameObject fakeBullet = null;

    MeshRenderer weaponMeshRenderer;
    LevelDataScriptableObject levelData;
    Animator weaponAnimator;
    AudioSource weaponSound = null;

    public void InitializeState()
    {
        switch (LevelController.Instance.GetLevelState)
        {
            case LevelController.LevelState.BLUE:
                {
                    ChangeState(LevelController.LevelState.BLUE);
                    break;
                }
            case LevelController.LevelState.RED:
                {
                    ChangeState(LevelController.LevelState.RED);
                    break;
                }
        }
    }

    public void ChangeState(LevelController.LevelState state)
    {
        switch (state)
        {
            case LevelController.LevelState.BLUE:
                {
                    weaponMeshRenderer.materials[0].SetTexture("_EmissionMap", weaponTextures[0]);
                    break;
                }
            case LevelController.LevelState.RED:
                {
                    weaponMeshRenderer.materials[0].SetTexture("_EmissionMap", weaponTextures[1]);
                    break;
                }
        }
    }

    public void HitFX(Vector3 hitPoint)
    {
        switch (LevelController.Instance.GetLevelState)
        {
            case LevelController.LevelState.BLUE:
                {
                    GameObject hitFX = Instantiate(shotHitFXBlue, hitPoint, shotHitFXBlue.transform.rotation) as GameObject;
                    break;
                }
            case LevelController.LevelState.RED:
                {
                    GameObject hitFX = Instantiate(shotHitFXRed, hitPoint, shotHitFXRed.transform.rotation) as GameObject;
                    break;
                }
        }
    }

    public void ShotFX()
    {
        GameObject weaponFX = Instantiate(shotFX, shotFXTransform.position, shotFXTransform.transform.rotation) as GameObject;
        GameObject fakeBulletFX = Instantiate(fakeBullet, gunBarrelEnd.transform.position, gunBarrelEnd.transform.rotation, gunBarrelEnd.transform) as GameObject;

        switch (LevelController.Instance.GetLevelState)
        {
            case LevelController.LevelState.BLUE:
                {
                    weaponFX.GetComponent<Light>().color = levelData.stateColors[0];
                    break;
                }
            case LevelController.LevelState.RED:
                {
                    weaponFX.GetComponent<Light>().color = levelData.stateColors[1];
                    break;
                }
        }

        weaponAnimator.SetTrigger("Shoot");
        weaponSound.Play();
    }

    void Awake()
    {
        weaponMeshRenderer = GetComponent<MeshRenderer>();
        levelData = GetComponent<ColorChangeComponent>().levelData;
        weaponAnimator = GetComponent<Animator>();
        weaponSound = weaponSoundObject.GetComponent<AudioSource>();

        LevelController.SwitchLevelStateEvent += ChangeState;

        currentAmmo = clipSize;
    }

    void OnEnable()
    {
        InitializeState();
    }

    void OnDestroy()
    {
        LevelController.SwitchLevelStateEvent -= ChangeState;
    }
}
