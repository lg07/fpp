﻿using UnityEngine;

public class MovementDependentBehaviour : StateMachineBehaviour
{
    public Rigidbody playerRigidbody = null;
    public PlayerMovement playerMovementComponent = null;
    public float playerSneakSpeed = 0;
    public float playerWalkSpeed = 0;

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (playerRigidbody && playerMovementComponent)
        {
            if (playerMovementComponent.CurrentMovementState == PlayerMovement.MovementState.SNEAKING)
            {
                animator.SetFloat("speedVar", playerRigidbody.velocity.magnitude / playerSneakSpeed);
            }
            else if (playerMovementComponent.CurrentMovementState == PlayerMovement.MovementState.WALKING)
            {
                animator.SetFloat("speedVar", playerRigidbody.velocity.magnitude / playerWalkSpeed);
            }
        }
    }
}
