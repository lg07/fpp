﻿using UnityEngine;
using System;

public class PlayerHealth : Health
{
    [SerializeField]
    float health = 100;
    bool isDead = false;

    public static event Action<float> PlayerHitEvent;
    void PlayerHit(float health)
    {
        if (PlayerHitEvent != null) PlayerHitEvent(health);
    }

    public override void ReceiveDamage(float damage, GameObject damageInstigator)
    {
        if (health > 0)
        {
            health -= damage;
            PlayerHit(health);

            if (health <= 0 && !isDead)
            {
                isDead = true;
                GetComponent<CapsuleCollider>().enabled = false;
                InputManager.Instance.SetPlayerInput(false);
                OnKilled();
            }
        }
    }

    void Start()
    {
        isDead = false;
        InputManager.Instance.SetPlayerInput(true);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("AIBullet"))
        {
            Destroy(other.gameObject);
        }
    }
}
