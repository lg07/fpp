﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public enum MovementState { IDLE, SNEAKING, WALKING, RUNNING, JUMPING };
    MovementState movementState = MovementState.IDLE;
    public MovementState CurrentMovementState
    {
        get
        {
            return movementState;
        }
    }

    float movementSpeed = 5000;
    [SerializeField]
    float maxSneakSpeed = 4;
    [SerializeField]
    float maxWalkSpeed = 7;
    [SerializeField]
    float maxRunSpeed = 12;
    [SerializeField]
    float sneakSpeedMultiplier = 0.5f;
    [SerializeField]
    float runSpeedMultiplier = 2f;
    float currentMovementSpeed;
    [SerializeField]
    float lookSpeed = 0;
    float actualLookSpeed = 0;
    [SerializeField]
    float lookRatio = 0;
    float actualLookRatio = 0;
    [SerializeField]
    float turnSpeed = 0;
    float actualTurnSpeed = 0;
    [SerializeField]
    float turnRatio = 0;
    float actualTurnRatio = 0;
    [SerializeField]
    float jumpForce = 0;
    [SerializeField]
    AudioSource jumpSound = null;

    Animator weaponAnimator;

    float mouseLookX = 0;
    float mouseLookY = 0;
    float gamepadLookX = 0;
    float gamepadLookY = 0;

    GameObject playerCamera;
    Rigidbody playerRigidbody;
    CapsuleCollider playerCapsuleCollider;
    Animator cameraAnimator;
    PlayerFootsteps playerFootsteps = null;
    int currentFootstep = 0;
    float footstepFrequency;
    float footstepCounter = 0;
    float jumpY = 10f;
    float baseDrag = 5f;

    void LookUp(float lookY)
    {
        playerCamera.transform.RotateAround(playerCamera.transform.position, playerCamera.transform.right, lookY);
        playerCamera.transform.localRotation = new Quaternion(Mathf.Clamp(playerCamera.transform.localRotation.x, -0.4f, 0.4f), 
            playerCamera.transform.localRotation.y, playerCamera.transform.localRotation.z, playerCamera.transform.localRotation.w);
    }

    void Turn(float lookX)
    {
        transform.RotateAround(transform.position, transform.up, lookX);
    }

    void Jump()
    {
        Vector3 jumpVector = new Vector3(playerRigidbody.velocity.x / movementSpeed, jumpY, playerRigidbody.velocity.z / movementSpeed);
        playerRigidbody.AddForce(jumpVector * jumpForce, ForceMode.Impulse);
        jumpSound.Play();
    }

    bool IsOnGround()
    {
        return Physics.Raycast(transform.position, Vector3.down, playerCapsuleCollider.height / 2 + 0.1f);
    }

    void SensitivitySettingsChanged()
    {
        actualLookSpeed = lookSpeed * (InputManager.Instance.mouseSensitivity / 50);
        actualLookRatio = lookRatio * (InputManager.Instance.gamepadSensitivity / 50);
        actualTurnSpeed = turnSpeed * (InputManager.Instance.mouseSensitivity / 50);
        actualTurnRatio = turnRatio * (InputManager.Instance.gamepadSensitivity / 50);
    }

    public void AssignWeapon(GameObject weapon)
    {
        weaponAnimator = weapon.GetComponent<Animator>();
        MovementDependentBehaviour[] movementDependentBehaviours = weaponAnimator.GetBehaviours<MovementDependentBehaviour>();

        foreach (var movementDependentBehaviour in movementDependentBehaviours)
        {
            movementDependentBehaviour.playerRigidbody = playerRigidbody;
            movementDependentBehaviour.playerSneakSpeed = maxSneakSpeed;
            movementDependentBehaviour.playerWalkSpeed = maxWalkSpeed;
            movementDependentBehaviour.playerMovementComponent = this;
        }
    }

    void Awake()
    {
        SensitivitySettings.SensitivitySettingsChangedEvent += SensitivitySettingsChanged;

        playerCamera = GameObject.FindGameObjectWithTag("MainCamera");
        playerCapsuleCollider = GetComponent<CapsuleCollider>();
        playerRigidbody = GetComponent<Rigidbody>();
        cameraAnimator = playerCamera.GetComponent<Animator>();
        playerFootsteps = playerCamera.GetComponent<PlayerFootsteps>();
    }

    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        mouseLookX = playerCamera.transform.localRotation.x;
        mouseLookY = playerCamera.transform.localRotation.y;
        gamepadLookX = playerCamera.transform.localRotation.x;
        gamepadLookY = playerCamera.transform.localRotation.y;

        SensitivitySettingsChanged();

        currentMovementSpeed = movementSpeed;
    }

    void OnDestroy()
    {
        SensitivitySettings.SensitivitySettingsChangedEvent -= SensitivitySettingsChanged;
    }

    void FixedUpdate()
    {       
        //MOVEMENT
        float sideMovement = Input.GetAxis("Horizontal");
        float forwardMovement = Input.GetAxis("Vertical");

        if (IsOnGround())
        {
            playerRigidbody.drag = baseDrag;

            if ((sideMovement < 0.1f && sideMovement > -0.1f && forwardMovement < 0.1f && forwardMovement > -0.1f) || !InputManager.Instance.PlayerInputActive
            || playerRigidbody.velocity.magnitude < 0.1f)
            {
                movementState = MovementState.IDLE;
                currentMovementSpeed = movementSpeed;
            }

            else if (movementState != MovementState.RUNNING && movementState != MovementState.SNEAKING 
                && playerRigidbody.velocity.magnitude >= 0.1f && InputManager.Instance.PlayerInputActive)
            {
                movementState = MovementState.WALKING;
                currentMovementSpeed = movementSpeed;
            }
        }

        else if (!IsOnGround())
        {
            playerRigidbody.drag = 1;
            movementState = MovementState.JUMPING;
        }

        switch (movementState)
        {
            case MovementState.IDLE:
                {
                    if (InputManager.Instance.PlayerInputActive)
                        playerRigidbody.AddForce(((sideMovement * transform.right) + (forwardMovement * transform.forward)) * currentMovementSpeed);

                    if (weaponAnimator) weaponAnimator.SetBool("IsMoving", false);
                    cameraAnimator.SetBool("IsMoving", false);
                    if (weaponAnimator) weaponAnimator.SetBool("IsRunning", false);
                    cameraAnimator.SetBool("IsRunning", false);

                    if (weaponAnimator) weaponAnimator.speed = 1;
                    cameraAnimator.speed = 1;

                    break;
                }
            case MovementState.SNEAKING:
                {
                    if (playerRigidbody.velocity.magnitude < maxSneakSpeed) //max sneaking speed
                        playerRigidbody.AddForce(((sideMovement * transform.right) + (forwardMovement * transform.forward)) * currentMovementSpeed);

                    if (weaponAnimator) weaponAnimator.SetBool("IsMoving", true);
                    cameraAnimator.SetBool("IsMoving", true);
                    if (weaponAnimator) weaponAnimator.SetBool("IsRunning", false);
                    cameraAnimator.SetBool("IsRunning", false);

                    cameraAnimator.speed = playerRigidbody.velocity.magnitude / maxSneakSpeed;

                    break;
                }
            case MovementState.WALKING:
                {
                    if (playerRigidbody.velocity.magnitude < maxWalkSpeed) //max walking speed
                        playerRigidbody.AddForce(((sideMovement * transform.right) + (forwardMovement * transform.forward)) * currentMovementSpeed);

                    if (weaponAnimator) weaponAnimator.SetBool("IsMoving", true);
                    cameraAnimator.SetBool("IsMoving", true);
                    if (weaponAnimator) weaponAnimator.SetBool("IsRunning", false);
                    cameraAnimator.SetBool("IsRunning", false);

                    cameraAnimator.speed = playerRigidbody.velocity.magnitude / maxWalkSpeed;

                    break;
                }
            case MovementState.RUNNING:
                {
                    if (playerRigidbody.velocity.magnitude < maxRunSpeed) //max running speed
                        playerRigidbody.AddForce(((sideMovement * transform.right) + (forwardMovement * transform.forward)) * currentMovementSpeed);

                    if (weaponAnimator) weaponAnimator.SetBool("IsMoving", true);
                    cameraAnimator.SetBool("IsMoving", true);
                    if (weaponAnimator) weaponAnimator.SetBool("IsRunning", true);
                    cameraAnimator.SetBool("IsRunning", true);

                    if (weaponAnimator) weaponAnimator.speed = 1;
                    cameraAnimator.speed = 1;

                    break;
                }
            case MovementState.JUMPING:
                {
                    playerRigidbody.AddForce(((sideMovement * transform.right) + (forwardMovement * transform.forward)) * currentMovementSpeed / 10);

                    if (weaponAnimator) weaponAnimator.SetBool("IsMoving", false);
                    cameraAnimator.SetBool("IsMoving", false);
                    if (weaponAnimator) weaponAnimator.SetBool("IsRunning", false);
                    cameraAnimator.SetBool("IsRunning", false);

                    if (weaponAnimator) weaponAnimator.speed = 1;
                    cameraAnimator.speed = 1;

                    break;
                }
        }
    }

    void LateUpdate()
    {
        //Look Up & Turn
        if (!InputManager.Instance.GamepadOn && InputManager.Instance.PlayerInputActive)
        {
            LookUp(mouseLookY);
            Turn(mouseLookX);
        }

        else if (InputManager.Instance.GamepadOn && InputManager.Instance.PlayerInputActive)
        {
            LookUp(gamepadLookY);
            Turn(gamepadLookX);
        }
    }

    void Update()
    {
        //LOOKING AROUND
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");
        float gamepadX = Input.GetAxis("PadLookX");
        float gamepadY = Input.GetAxis("PadLookY");

        mouseLookX = mouseX * actualTurnSpeed * Time.deltaTime * (1920f/Screen.width);
        if (Mathf.Abs(gamepadX) > 0.01f) gamepadLookX = gamepadX * actualTurnRatio * Time.deltaTime;
        else gamepadLookX = 0;

        mouseLookY = -mouseY * actualLookSpeed * Time.deltaTime * (1080f/Screen.height);
        if (Mathf.Abs(gamepadY) > 0.01f) gamepadLookY = gamepadY * actualLookRatio * Time.deltaTime;
        else gamepadLookY = 0;

        //JUMP
        if (InputManager.Instance.PlayerInputActive && Input.GetButtonDown("Jump") && IsOnGround())
        {
            Jump();
        }

        //SNEAKING
        float forwardMovement = Input.GetAxis("Vertical");

        if (Input.GetButton("Sneak") && InputManager.Instance.PlayerInputActive && IsOnGround())
        {
            movementState = MovementState.SNEAKING;
            currentMovementSpeed = movementSpeed * sneakSpeedMultiplier;
        }

        else if (movementState == MovementState.SNEAKING && (Input.GetButtonUp("Sneak") || !InputManager.Instance.PlayerInputActive || !IsOnGround() 
            || playerRigidbody.velocity.magnitude < 0.1f))
        {
            movementState = MovementState.IDLE;
            currentMovementSpeed = movementSpeed;
        }

        //RUNNING
        else if (Input.GetButton("Run") && forwardMovement > 0 && InputManager.Instance.PlayerInputActive && IsOnGround() && playerRigidbody.velocity.magnitude >= 0.1f)
        {           
            movementState = MovementState.RUNNING;
            currentMovementSpeed = movementSpeed * runSpeedMultiplier;
        }

        else if (movementState == MovementState.RUNNING && (Input.GetButtonUp("Run") || forwardMovement <= 0 || !InputManager.Instance.PlayerInputActive || !IsOnGround() 
            || playerRigidbody.velocity.magnitude < 0.1f))
        {
            movementState = MovementState.IDLE;
            currentMovementSpeed = movementSpeed;          
        }

        //FOOTSTEPS SFX
        if (IsOnGround() && playerRigidbody.velocity.magnitude > maxSneakSpeed + 1f)
        {
            footstepFrequency = (1 / playerRigidbody.velocity.magnitude) * 3.5f;
            footstepCounter += Time.deltaTime;
            if (footstepCounter > footstepFrequency)
            {
                if (currentFootstep == 0)
                {
                    playerFootsteps.PlayFoostepSound(1);
                    currentFootstep = 1;
                }
                else
                {
                    playerFootsteps.PlayFoostepSound(0);
                    currentFootstep = 0;
                }
                footstepCounter = 0;
            }
        }
    }
}
