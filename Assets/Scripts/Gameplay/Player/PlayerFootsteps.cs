﻿using UnityEngine;
using System;

public class PlayerFootsteps : MonoBehaviour
{
    [SerializeField]
    AudioSource leftFootstep = null;
    [SerializeField]
    AudioSource rightFootstep = null;

    public static event Action<GameObject> PawnSoundEvent;
    void PawnSound()
    {
        if (PawnSoundEvent != null) PawnSoundEvent(transform.root.gameObject);
    }

    public void PlayFoostepSound(int index)
    {
        switch (index)
        {
            case 0:
                {
                    leftFootstep.Play();
                    break;
                }
            case 1:
                {
                    rightFootstep.Play();
                    break;
                }
        }
        PawnSound();
    }
}
