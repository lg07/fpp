﻿using System.Collections;
using System;
using UnityEngine;

//Controls weapons equipped by Player

public class PlayerWeapon : MonoBehaviour
{
    public WeaponsScriptableObject weaponsScriptableObject = null;

    [SerializeField]
    Transform bulletSpawnPoint = null;
    float fireInterval = 0;

    [SerializeField]
    Transform weaponPosition = null;

    [SerializeField]
    AudioSource weaponSound = null;

    [SerializeField]
    AudioSource hackSound = null;

    GameObject currentWeapon = null;
    WeaponItem[] weapons = new WeaponItem[2];
    WeaponItem currentWeaponItemComponent = null;
    bool isSwitchingWeapon = false;
    int currentWeaponID = 0;
    GameObject[] actualWeapons = new GameObject[2];

    PlayerMovement playerMovementComponent = null;
    GameObject mainCamera = null;

    IFF iffComponent = null;

    LineRenderer hackingDeviceLineRenderer = null;
    float hackingProgress = 0;

    public static event Action<float> UpdateHackingProgressOnHUDEvent;
    void UpdateHackingProgressOnHUD(float hackingProgress)
    {
        if (UpdateHackingProgressOnHUDEvent != null) UpdateHackingProgressOnHUDEvent(hackingProgress);
    }

    public static event Action<string> WeaponChangedEvent;
    void WeaponChanged(string weaponName)
    {
        if (WeaponChangedEvent != null) WeaponChangedEvent(weaponName);
    }

    public static event Action<string, string> UpdateAmmoEvent;
    void UpdateAmmo(string weaponAmmoLabel, string currentAmmo)
    {
        if (UpdateAmmoEvent != null) UpdateAmmoEvent(weaponAmmoLabel, currentAmmo);
    }

    IEnumerator SimulateProjectileWithRaycast()
    {
        float projectileVelocity = 50;
        float cachedDamage = currentWeaponItemComponent.Damage;
        WeaponItem.WeaponType cachedWeaponType = currentWeaponItemComponent.weaponType;

        RaycastHit hit;

        if (Physics.Raycast(bulletSpawnPoint.position, bulletSpawnPoint.forward, out hit, 1000))
        {
            yield return new WaitForSeconds(Vector2.Distance(bulletSpawnPoint.position, hit.point) / projectileVelocity);

            if (hit.transform)
            {
                if (iffComponent.IsTargetHostile(hit.transform.gameObject))
                {
                    hit.transform.gameObject.GetComponent<Health>().ReceiveDamage(cachedDamage, gameObject);
                }

                currentWeaponItemComponent.HitFX(hit.point);
            }
        }
    }

    void Hack()
    {
        RaycastHit hit;

        if (!hackSound.isPlaying) hackSound.Play();
        hackSound.pitch = (hackingProgress / 100) + 1;

        if (Physics.Raycast(bulletSpawnPoint.position, bulletSpawnPoint.forward, out hit, 100))
        {
            hackingDeviceLineRenderer.SetPosition(0, currentWeaponItemComponent.gunBarrelEnd.position);
            hackingDeviceLineRenderer.SetPosition(1, hit.point);

            if (iffComponent.IsTargetHostile(hit.transform.gameObject) && LevelController.Instance.GetLevelState == LevelController.LevelState.RED)
            {
                if (hackingProgress < 100)
                {
                    hackingProgress += Time.deltaTime * 25;
                    UpdateHackingProgressOnHUD(hackingProgress);
                }
                else if (hackingProgress >= 100)
                {
                    hit.transform.gameObject.GetComponent<IFF>().InitializeHackedState(iffComponent.IFF_group);
                    hit.transform.gameObject.GetComponent<AITargetDetection>().PlayHackedSound();
                    currentWeaponItemComponent.CurrentAmmo--;
                    UpdateAmmoOnHUD();
                    StopHacking();
                }
            }
            else
            {
                hackingProgress = 0;
            }
        }
        else
        {
            hackingDeviceLineRenderer.SetPosition(0, currentWeaponItemComponent.gunBarrelEnd.position);
            hackingDeviceLineRenderer.SetPosition(1, bulletSpawnPoint.position + (bulletSpawnPoint.forward * 100));
        }
    }

    void StopHacking()
    {

        hackingDeviceLineRenderer.SetPosition(0, bulletSpawnPoint.position);
        hackingDeviceLineRenderer.SetPosition(1, bulletSpawnPoint.position);
        hackingProgress = 0;
        UpdateHackingProgressOnHUD(hackingProgress);
        hackSound.Stop();
    }

    void UpdateAmmoOnHUD()
    {
        if (currentWeaponItemComponent.ClipSize > 0) UpdateAmmo(currentWeaponItemComponent.AmmoLabel, currentWeaponItemComponent.CurrentAmmo.ToString());
        else UpdateAmmo(currentWeaponItemComponent.AmmoLabel, "INF");
    }

    void OnPlayerKilled(GameObject killed)
    {
        if (killed == gameObject)
        {
            if (currentWeaponItemComponent.weaponType == WeaponItem.WeaponType.HACKING) StopHacking();
        }
    }

    void CycleWeapon(bool next)
    {
        if (next)
        {
            if (currentWeaponID < weapons.Length - 1)
            {
                SelectWeapon(currentWeaponID + 1);
            }
            else
            {
                SelectWeapon(0);
            }
        }
        else
        {
            if (currentWeaponID > 0)
            {
                SelectWeapon(currentWeaponID - 1);
            }
            else
            {
                SelectWeapon(weapons.Length - 1);
            }
        }
    }

    IEnumerator WeaponSwitching(int id)
    {
        isSwitchingWeapon = true;
        if (currentWeapon)
        {
            currentWeapon.GetComponent<Animator>().SetTrigger("WeaponChange");
            yield return new WaitForSeconds(0.35f);
            currentWeapon.SetActive(false);
            currentWeapon.transform.SetPositionAndRotation(weaponPosition.position, weaponPosition.rotation);
        }
        yield return new WaitForSeconds(0.2f);
        currentWeapon = actualWeapons[id];
        currentWeapon.SetActive(true);
        currentWeapon.GetComponent<Animator>().SetTrigger("WeaponEquip");
        playerMovementComponent.AssignWeapon(currentWeapon);
        currentWeaponItemComponent = currentWeapon.GetComponent<WeaponItem>();
        fireInterval = currentWeaponItemComponent.FireRate;

        WeaponChanged(currentWeaponItemComponent.weaponName);
        UpdateAmmoOnHUD();

        hackingDeviceLineRenderer = null;
        if (currentWeaponItemComponent.weaponType == WeaponItem.WeaponType.HACKING) hackingDeviceLineRenderer = currentWeapon.GetComponentInChildren<LineRenderer>();

        weaponSound.Play();

        yield return new WaitForSeconds(0.25f);
        currentWeaponID = id;
        isSwitchingWeapon = false;
    }

    void SelectWeapon(int id)
    {
        if ((currentWeaponID != id || !currentWeapon) && !isSwitchingWeapon)
        {
            if (currentWeaponItemComponent)
            {
                if (currentWeaponItemComponent.weaponType == WeaponItem.WeaponType.HACKING)
                    StopHacking();
            }

            StartCoroutine(WeaponSwitching(id));         
        }
    }

    void Awake()
    {
        PlayerHealth.OnKilledEvent += OnPlayerKilled;
        iffComponent = GetComponent<IFF>();
        playerMovementComponent = GetComponent<PlayerMovement>();
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
    }

    void Start()
    {
        weapons[0] = weaponsScriptableObject.weapons[0]; //default weapon
        weapons[1] = weaponsScriptableObject.weapons[1]; //hacking device

        for (int i = 0; i < actualWeapons.Length; i++)
        {
            actualWeapons[i] = Instantiate(weapons[i].gameObject, weaponPosition.position, weaponPosition.rotation, mainCamera.transform) as GameObject;
            actualWeapons[i].SetActive(false);
        }

        SelectWeapon(0);
        hackingProgress = 0;
    }

    void OnDestroy()
    {
        PlayerHealth.OnKilledEvent -= OnPlayerKilled;
    }

    void Update()
    {
        //Using weapon
        if (currentWeapon)
        {
            if (fireInterval < currentWeaponItemComponent.FireRate + 1) /*so it won't increment endlessly*/ fireInterval += Time.deltaTime;

            if (InputManager.Instance.PlayerInputActive && !isSwitchingWeapon)
            {
                if ((Input.GetButton("Fire1") || Input.GetAxis("Fire2") > 0) && (currentWeaponItemComponent.CurrentAmmo > 0 || currentWeaponItemComponent.ClipSize == 0))
                {
                    if (fireInterval > currentWeaponItemComponent.FireRate && currentWeaponItemComponent.weaponType == WeaponItem.WeaponType.COMBAT)
                    {
                        fireInterval = 0;
                        currentWeaponItemComponent.ShotFX();
                        StartCoroutine(SimulateProjectileWithRaycast());
                        if (currentWeaponItemComponent.ClipSize > 0)
                        {
                            currentWeaponItemComponent.CurrentAmmo--;
                            UpdateAmmoOnHUD();
                        }
                    }
                    else if (currentWeaponItemComponent.weaponType == WeaponItem.WeaponType.HACKING)
                    {
                        Hack();
                    }
                }

                else if ((Input.GetButtonUp("Fire1") || (Input.GetAxis("Fire2") == 0 && InputManager.Instance.GamepadOn)) && currentWeaponItemComponent.weaponType == WeaponItem.WeaponType.HACKING)
                {
                    StopHacking();
                }

                if (Input.GetKeyDown(KeyCode.Alpha1))
                {
                    SelectWeapon(0);
                }
                else if (Input.GetKeyDown(KeyCode.Alpha2))
                {
                    SelectWeapon(1);
                }

                else if (Input.GetAxis("Mouse ScrollWheel") > 0)
                {
                    CycleWeapon(true);
                }
                else if (Input.GetAxis("Mouse ScrollWheel") < 0)
                {
                    CycleWeapon(false);
                }
                else if (Input.GetButtonDown("GamepadWeaponCycle"))
                {
                    CycleWeapon(true);
                }
            }
        }
    }
}
