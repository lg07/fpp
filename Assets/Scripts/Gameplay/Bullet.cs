﻿using UnityEngine;

public class Bullet : MonoBehaviour, IStateDependent
{
    [SerializeField]
    float damage = 10;
    public float Damage
    {
        get
        {
            return damage;
        }
    }

    public LevelDataScriptableObject levelData;

    TrailRenderer bulletTrail;

    public void InitializeState()
    {
        switch (LevelController.Instance.GetLevelState)
        {
            case LevelController.LevelState.BLUE:
                {
                    ChangeState(LevelController.LevelState.BLUE);
                    break;
                }
            case LevelController.LevelState.RED:
                {
                    ChangeState(LevelController.LevelState.RED);
                    break;
                }
        }
    }

    public void ChangeState(LevelController.LevelState state)
    {
        switch (state)
        {
            case LevelController.LevelState.BLUE:
                {
                    bulletTrail.materials[0].SetColor("_EmissionColor", levelData.stateColors[0]);
                    break;
                }
            case LevelController.LevelState.RED:
                {
                    bulletTrail.materials[0].SetColor("_EmissionColor", levelData.stateColors[1]);
                    break;
                }
        }
    }

    void Awake()
    {
        bulletTrail = GetComponent<TrailRenderer>();
    }

    void Start()
    {
        InitializeState();
    }

    void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag("AIPawn") && !other.gameObject.CompareTag("Player") && !other.gameObject.CompareTag("Weapon"))
        {
            Destroy(gameObject);      
        }
    }
}
