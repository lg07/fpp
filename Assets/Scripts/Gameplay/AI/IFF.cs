﻿using System.Collections.Generic;
using System.Collections;
using System;
using UnityEngine;

//Component responsible for distinguishing friendlies and hostiles (IFF = Identification Friend or Foe)

public class IFF : MonoBehaviour
{
    public enum IFFGroup { GROUP_A, GROUP_B, GROUP_C, GROUP_D };
    public IFFGroup IFF_group = IFFGroup.GROUP_B;
    IFFGroup initial_IFF_Group;
    [SerializeField]
    [Tooltip("How long remains hacked")]
    float hackedTime = 60;
    public float HackedTime { get { return hackedTime; } }
    bool hacked = false;
    public bool Hacked { get { return hacked; } }
    float remainingHackedTime;
    public float RemainingHackedTime { get { return remainingHackedTime; }}
    List<GameObject> IFFTargetsList = new List<GameObject>();

    public static event Action HackedEvent;
    void ToggleHack()
    {
        if (HackedEvent != null) HackedEvent();
        hacked = !hacked;

        if (hacked)
            remainingHackedTime = hackedTime;
        else
            remainingHackedTime = 0;
    }

    public List<GameObject> GetIFFTargetsList()
    {
        return IFFTargetsList;
    }

    public bool IsValidTarget(GameObject target)
    {
        return IFFTargetsList.Contains(target);
    }

    public bool IsTargetHostile(GameObject target)
    {
        if (target.GetComponent<IFF>())
            return target.GetComponent<IFF>().IFF_group != IFF_group;
        else
            return false;
    }

    void InitializeIFF()
    {
        IFF[] IFFTargets = FindObjectsOfType<IFF>();

        foreach (var IFFTarget in IFFTargets)
        {
            IFFTargetsList.Add(IFFTarget.gameObject);
        }

        if (GetComponent<AITargetDetection>()) GetComponent<AITargetDetection>().InitializeState();
    }

    void OnKilled(GameObject killedPawn)
    {
        IFFTargetsList.Remove(killedPawn);
    }

    void UpdateIFF()
    {
        IFFTargetsList.Clear();
        InitializeIFF();
    }

    public void InitializeHackedState(IFFGroup Hacker_IFF_group)
    {
        IFF_group = Hacker_IFF_group;
        ToggleHack();
    }

    void Awake()
    {
        Health.OnKilledEvent += OnKilled;
        AISpawner.AISpawnedEvent += UpdateIFF;
        HackedEvent += UpdateIFF;
    }

    void OnDestroy()
    {
        Health.OnKilledEvent -= OnKilled;
        AISpawner.AISpawnedEvent -= UpdateIFF;
        HackedEvent -= UpdateIFF;
    }

    void Start()
    {
        initial_IFF_Group = IFF_group;
        InitializeIFF();
    }

    void Update()
    {
        if (hacked)
        {
            remainingHackedTime -= Time.deltaTime;
            if (remainingHackedTime <= 0)
            {
                IFF_group = initial_IFF_Group;
                ToggleHack();
            }
        }
    }
}
