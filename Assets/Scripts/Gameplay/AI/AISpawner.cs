﻿using System.Collections;
using UnityEngine;
using System;

//Spawns back killed AI Drones
public class AISpawner : MonoBehaviour
{
    [SerializeField]
    GameObject AIPawn = null;
    [SerializeField]
    float respawnDelay = 10;
    WaitForSeconds respawnDelayWait;

    bool active;

    public static event Action AISpawnedEvent;
    void AISpawned()
    {
        if (AISpawnedEvent != null) AISpawnedEvent();
    }

    IEnumerator RespawnAIPawn(Waypoint startWaypoint)
    {
        yield return respawnDelayWait;
        if (active)
        {
            GameObject spawnedAIPawn = Instantiate(AIPawn, transform.position, transform.rotation) as GameObject;
            spawnedAIPawn.GetComponent<AIMovement>().startWaypoint = startWaypoint;
            AISpawned();
        }
    }

    void SpawnAI(GameObject killedPawn)
    {
        if (killedPawn.GetComponent<AIHealth>())
        {
            if (killedPawn.GetComponent<AIHealth>().Respawnable)
            {
                StartCoroutine(RespawnAIPawn(killedPawn.GetComponent<AIMovement>().startWaypoint));
            }
        }
    }

    void Deactivate()
    {
        active = false;
    }

    void Awake ()
    {
        Health.OnKilledEvent += SpawnAI;
        LevelController.GameOverOnUIEvent += Deactivate;
        LevelController.VictoryOnUIEvent += Deactivate;
    }
	
	void OnDestroy ()
    {
        Health.OnKilledEvent -= SpawnAI;
        LevelController.GameOverOnUIEvent -= Deactivate;
        LevelController.VictoryOnUIEvent -= Deactivate;
    }

    void Start()
    {
        active = true;
        respawnDelayWait = new WaitForSeconds(respawnDelay);
    }
}
