﻿using UnityEngine;
using System;

public class AIHealth : Health, IStateDependent
{
    [SerializeField]
    GameObject enemyExplosion = null;

    [SerializeField]
    bool respawnable = true;
    public bool Respawnable { get { return respawnable; } }

    [SerializeField]
    float health = 100;
    public float Health { get { return health; } }

    bool invulnerable = true;
    AITargetDetection AITargetDetectionComponent;
    bool isAlive;

    public void InitializeState()
    {
        switch (LevelController.Instance.GetLevelState)
        {
            case LevelController.LevelState.BLUE:
                {
                    ChangeState(LevelController.LevelState.BLUE);
                    break;
                }
            case LevelController.LevelState.RED:
                {
                    ChangeState(LevelController.LevelState.RED);
                    break;
                }
        }
    }

    public void ChangeState(LevelController.LevelState state)
    {
        switch (state)
        {
            case LevelController.LevelState.BLUE:
                {
                    invulnerable = false;
                    break;
                }
            case LevelController.LevelState.RED:
                {
                    invulnerable = true;
                    break;
                }
        }
    }

    void DestroyEnemy()
    {
        GameObject explosion = Instantiate(enemyExplosion, transform.position, transform.rotation) as GameObject;
        Destroy(gameObject);
    }

    void DestroyEnemyOnVictory()
    {
        if (isAlive)
        {
            isAlive = false;
            GameObject explosion = Instantiate(enemyExplosion, transform.position, transform.rotation) as GameObject;
            Destroy(gameObject);
        }
    }

    public override void ReceiveDamage(float damage, GameObject damageInstigator) //invoked in PlayerWeapon with raycast
    {
        AITargetDetectionComponent.OnShotReceived(damageInstigator);
        
        //TODO sound FX + shake
        if (!invulnerable || damageInstigator.CompareTag("AIPawn"))
        {
            health -= damage;

            if (health <= 0 && isAlive)
            {
                isAlive = false;
                OnKilled();
                DestroyEnemy();
            }
        }
    }

    void Awake()
    {
        LevelController.SwitchLevelStateEvent += ChangeState;
        LevelController.VictoryOnUIEvent += DestroyEnemyOnVictory;
        AITargetDetectionComponent = GetComponent<AITargetDetection>();
    }

    void OnDestroy()
    {
        LevelController.SwitchLevelStateEvent -= ChangeState;
        LevelController.VictoryOnUIEvent -= DestroyEnemyOnVictory;
    }

    void Start()
    {
        InitializeState();
        isAlive = true;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("AIBullet"))
        {
            Destroy(other.gameObject);
        }
    }
}
