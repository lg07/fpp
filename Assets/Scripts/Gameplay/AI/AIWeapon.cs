﻿using UnityEngine;
using System.Collections;

public class AIWeapon : MonoBehaviour
{
    public GameObject gunBarrelEnd;
    public GameObject bullet;

    [SerializeField]
    float damage = 10;
    [SerializeField]
    float fireRate = 0;
    float fireInterval = 0;

    [SerializeField]
    float combatRange = 8;

    AITargetDetection enemyPlayerDetectionComponent;

    [SerializeField]
    AudioSource shotSound = null;

    [SerializeField]
    GameObject weaponShotFX = null;
    [SerializeField]
    Transform shotFXTransform = null;

    IFF iffComponent = null;

    public bool TargetInSight()
    {
        RaycastHit hit;
        int ignoredLayerMask = ~(1 << 8); //shoot from behind spawner barriers
        return (Physics.Raycast(gunBarrelEnd.transform.position, gunBarrelEnd.transform.forward, out hit, combatRange, ignoredLayerMask) && hit.transform.gameObject == enemyPlayerDetectionComponent.CurrentTarget);
    }

    IEnumerator SimulateProjectileWithRaycast()
    {
        float projectileVelocity = 50;

        RaycastHit hit;

        if (Physics.Raycast(gunBarrelEnd.transform.position, gunBarrelEnd.transform.forward, out hit, 1000))
        {
            yield return new WaitForSeconds(Vector2.Distance(gunBarrelEnd.transform.position, hit.point) / projectileVelocity);

            if (hit.transform)
            {
                if (iffComponent.IsTargetHostile(hit.transform.gameObject))
                {
                    hit.transform.gameObject.GetComponent<Health>().ReceiveDamage(damage, gameObject);
                }
            }
        }
    }

    void Awake()
    {
        enemyPlayerDetectionComponent = GetComponent<AITargetDetection>();
        iffComponent = GetComponent<IFF>();
    }

    void Update ()
    {
        if (enemyPlayerDetectionComponent.GetAwarenessState == AITargetDetection.AwarenessState.ENGAGING && TargetInSight() && fireInterval > fireRate)
        {
            fireInterval = 0;

            //Fake projectile
            GameObject projectile = Instantiate(bullet, gunBarrelEnd.transform.position, gunBarrelEnd.transform.rotation) as GameObject;
            GameObject shotFX = Instantiate(weaponShotFX, shotFXTransform.position, shotFXTransform.transform.rotation) as GameObject;
            projectile.GetComponent<Rigidbody>().velocity = transform.forward * 50;

            //Actual shot
            StartCoroutine(SimulateProjectileWithRaycast());
            shotSound.Play();
        }
        if (fireInterval < fireRate + 1) /*so it won't increment endlessly*/ fireInterval += Time.deltaTime;
    }
}
