﻿using UnityEngine;
using UnityEngine.AI;

public class AIMovement : MonoBehaviour
{
    NavMeshAgent agent;

    public Waypoint startWaypoint = null;
    Waypoint currentWaypoint = null;

    [SerializeField]
    float minDistanceFromPlayer = 5;

    [SerializeField]
    float rotateToPlayerRatio = 100;

    [SerializeField]
    float chaseSpeedMultiplier = 2f;

    [SerializeField]
    float destinationTolerance = 0.3f;
    
    [SerializeField]
    float searchRotationMultiplier = 25f;

    AITargetDetection AITargetDetectionComponent;
    float normalSpeed;

    bool DestinationReached(Vector3 destination)
    {
        return Vector3.Distance(new Vector3(transform.position.x, transform.position.y - agent.height / 2, transform.position.z), destination) < destinationTolerance;
    }

    void FollowCurrentWaypoint()
    {
        agent.destination = currentWaypoint.transform.position;
    }

    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        AITargetDetectionComponent = GetComponent<AITargetDetection>();
    }

    void Start()
    {
        normalSpeed = agent.speed;
        if (startWaypoint) currentWaypoint = startWaypoint;
    }

    void Update()
    {
        switch (AITargetDetectionComponent.GetAwarenessState)
        {
            case AITargetDetection.AwarenessState.ENGAGING:
                {
                    if (Vector3.Distance(transform.position, AITargetDetectionComponent.CurrentTarget.transform.position) > minDistanceFromPlayer || AITargetDetectionComponent.TargetBehindObstacle(AITargetDetectionComponent.CurrentTarget))
                    {
                        agent.destination = AITargetDetectionComponent.CurrentTarget.transform.position;
                        agent.speed = normalSpeed * chaseSpeedMultiplier;
                    }

                    else if (Vector3.Distance(transform.position, AITargetDetectionComponent.CurrentTarget.transform.position) <= minDistanceFromPlayer && !AITargetDetectionComponent.TargetBehindObstacle(AITargetDetectionComponent.CurrentTarget))
                    {
                        agent.ResetPath();
                        //ROTATING TO TARGET
                        Vector3 relativePos = AITargetDetectionComponent.CurrentTarget.transform.position - transform.position;
                        Quaternion targetRotation = Quaternion.LookRotation(relativePos);
                        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, Time.deltaTime * rotateToPlayerRatio);
                    }
                    break;
                }
            case AITargetDetection.AwarenessState.SEARCHING:
                {
                    agent.destination = AITargetDetectionComponent.TargetLastKnownPosition;
                    if (DestinationReached(AITargetDetectionComponent.TargetLastKnownPosition)) transform.RotateAround(transform.position, transform.up, Time.deltaTime * searchRotationMultiplier);
                    break;
                }
            case AITargetDetection.AwarenessState.UNAWARE:
                {
                    if (currentWaypoint)
                    {
                        agent.speed = normalSpeed;
                        FollowCurrentWaypoint();

                        if (DestinationReached(currentWaypoint.transform.position) && currentWaypoint.nextWaypoint)
                        {
                            currentWaypoint = currentWaypoint.nextWaypoint;
                        }

                        else if (DestinationReached(currentWaypoint.transform.position) && !currentWaypoint.nextWaypoint)
                        {
                            currentWaypoint = null;
                        }
                    }
                    break;
                }
        }
    }
}
