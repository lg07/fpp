﻿using UnityEngine;
using UnityEngine.AI;
using System;

//Component handles AI reaction to hostile targets

public class AITargetDetection : MonoBehaviour, IStateDependent
{
    GameObject playerCharacter = null;
    NavMeshAgent agent = null;

    enum SensingState { AGGRESSIVE, CARELESS };
    SensingState sensingState = SensingState.AGGRESSIVE;

    public enum AwarenessState { ENGAGING, SEARCHING, UNAWARE };
    AwarenessState awarenessState = AwarenessState.UNAWARE;
    public AwarenessState GetAwarenessState { get { return awarenessState; } }

    [SerializeField]
    float detectionRange = 10;
    [SerializeField]
    float detectionAngle = 70;

    [SerializeField]
    [Tooltip("How long should the enemy search for target, when reached last known target's position")]
    float searchingTime = 5;
    float searchTimer = 0;

    [SerializeField]
    [Tooltip("How long should the enemy chase target, when target is hidden")]
    float engageTime = 3;
    float engageTimer = 0;

    [SerializeField]
    Light searchlight = null;
    public LevelDataScriptableObject levelData;

    [SerializeField]
    AudioSource hackedSound = null;

    Vector3 targetLastKnownPosition;
    public Vector3 TargetLastKnownPosition { get { return targetLastKnownPosition; } }

    ColorChangeComponent colorChangeComponent;

    IFF iffComponent = null;
    GameObject currentTarget = null;
    public GameObject CurrentTarget { get { return currentTarget; } }
    bool IFFInitialized = false;

    //event used in MusicManager to play/stop action music
    public static event Action<AwarenessState> AwarenessStateChangedEvent;
    void AwarenessStateChanged(AwarenessState state)
    {
        if (AwarenessStateChangedEvent != null) AwarenessStateChangedEvent(state);
    }

    bool IsInPlayerTeam()
    {
        return iffComponent.IFF_group == playerCharacter.GetComponent<IFF>().IFF_group;
    }

    //TODO - optimize to check only targets within certain radius
    void SearchForTargets()
    {
        foreach (var target in iffComponent.GetIFFTargetsList())
        {
            if (iffComponent.IsTargetHostile(target))
            {
                if (Vector3.Distance(transform.position, target.transform.position) < detectionRange
                && Vector3.Angle(transform.forward, (target.transform.position - transform.position)) < detectionAngle && !TargetBehindObstacle(target.gameObject))
                {
                    currentTarget = target.gameObject;
                    break;
                }
            }
        }
    }

    //TODO - why not use it in SearchForTargets method?
    bool TargetVisibile(GameObject target)
    {
            return Vector3.Distance(transform.position, target.transform.position) < detectionRange
                    && Vector3.Angle(transform.forward, (target.transform.position - transform.position)) < detectionAngle && !TargetBehindObstacle(target.gameObject);
    }

    void ReactToSound(GameObject soundCauser)
    {
        if (iffComponent.IsTargetHostile(soundCauser))
        {
            if (sensingState == SensingState.AGGRESSIVE && awarenessState != AwarenessState.ENGAGING && Vector3.Distance(transform.position, soundCauser.transform.position) <= detectionRange)
            {
                currentTarget = soundCauser;
                Engage();
            }
        }
    }

    bool TargetLastKnownPositionReached()
    {
        return Vector3.Distance(new Vector3(transform.position.x, transform.position.y - agent.height / 2, transform.position.z), targetLastKnownPosition) < 0.4f;
    }

    public bool TargetBehindObstacle(GameObject target)
    {
        if (target)
        {
            RaycastHit obstacleCheck;

            int ignoredLayerMask = ~(1 << 8);

            return Physics.Raycast(transform.position, target.transform.position - transform.position, out obstacleCheck, Vector3.Distance(transform.position, target.transform.position), ignoredLayerMask)
                && obstacleCheck.transform.gameObject != target;
        }
        else
            return true;
    }

    void Engage()
    {
        awarenessState = AwarenessState.ENGAGING;
        if (currentTarget == playerCharacter) AwarenessStateChanged(AwarenessState.ENGAGING);
        searchTimer = 0;
        engageTimer = 0;
    }

    void ChaseTarget()
    {
        engageTimer += Time.deltaTime;
        if (engageTimer > engageTime)
        {
            targetLastKnownPosition =
                new Vector3(currentTarget.transform.position.x, currentTarget.transform.position.y - currentTarget.GetComponent<CapsuleCollider>().height / 2, currentTarget.transform.position.z);
            awarenessState = AwarenessState.SEARCHING;
            if (currentTarget == playerCharacter) AwarenessStateChanged(AwarenessState.SEARCHING);
        }
    }

    void CheckLastKnownPosition()
    {
        searchTimer += Time.deltaTime;
        if (searchTimer > searchingTime) BecomeUnaware();
    }

    void BecomeUnaware()
    {
        if (awarenessState != AwarenessState.SEARCHING && awarenessState != AwarenessState.UNAWARE && currentTarget == playerCharacter)
            AwarenessStateChanged(AwarenessState.UNAWARE);

        if (awarenessState != AwarenessState.UNAWARE) awarenessState = AwarenessState.UNAWARE;
        if (searchTimer != 0) searchTimer = 0;
        if (currentTarget) currentTarget = null;
    }

    void OnBecomeFriendly()
    {
        colorChangeComponent.enabled = false;
        GetComponentInChildren<MeshRenderer>().materials[colorChangeComponent.EmissiveMaterialIndex].SetColor("_EmissionColor", levelData.stateColors[2]);
        searchlight.color = levelData.stateColors[2];
    }

    public void ChangeState(LevelController.LevelState state)
    {
        if (!IsInPlayerTeam())
        {
            switch (state)
            {
                case LevelController.LevelState.BLUE:
                    {
                        sensingState = SensingState.CARELESS;
                        searchlight.color = levelData.stateColors[0];
                        break;
                    }
                case LevelController.LevelState.RED:
                    {
                        sensingState = SensingState.AGGRESSIVE;
                        searchlight.color = levelData.stateColors[1];
                        break;
                    }
            }
        }
    }

    public void InitializeState()
    {
        if (IsInPlayerTeam())
        {
            OnBecomeFriendly();
        }

        else
        {
            colorChangeComponent.enabled = true;
            switch (LevelController.Instance.GetLevelState)
            {
                case LevelController.LevelState.BLUE:
                    {
                        sensingState = SensingState.CARELESS;
                        colorChangeComponent.ChangeState(LevelController.LevelState.BLUE);
                        searchlight.color = levelData.stateColors[0];
                        break;
                    }
                case LevelController.LevelState.RED:
                    {
                        sensingState = SensingState.AGGRESSIVE;
                        colorChangeComponent.ChangeState(LevelController.LevelState.RED);
                        searchlight.color = levelData.stateColors[1];
                        break;
                    }
            }
        }
        IFFInitialized = true;
    }

    void OnKilled(GameObject killedPawn)
    {
        if (killedPawn == currentTarget || killedPawn == gameObject)
        {
            BecomeUnaware();
        }
    }

    public void OnShotReceived(GameObject damageInstigator)
    {
        if (sensingState == SensingState.AGGRESSIVE && awarenessState != AwarenessState.ENGAGING)
        {
            currentTarget = damageInstigator;
            Engage();
        }
    }

    public void PlayHackedSound()
    {
        hackedSound.Play();
    }

    void Awake()
    {
        playerCharacter = GameObject.FindGameObjectWithTag("Player");
        agent = GetComponent<NavMeshAgent>();
        colorChangeComponent = GetComponentInChildren<ColorChangeComponent>();
        iffComponent = GetComponent<IFF>();

        LevelController.SwitchLevelStateEvent += ChangeState;
        Health.OnKilledEvent += OnKilled;
        PlayerFootsteps.PawnSoundEvent += ReactToSound;
    }

    void OnDestroy()
    {
        LevelController.SwitchLevelStateEvent -= ChangeState;
        Health.OnKilledEvent -= OnKilled;
        PlayerFootsteps.PawnSoundEvent -= ReactToSound;
    }

    void Update()
    {
        //STATE MACHINE
        if (IFFInitialized)
        {
            if (sensingState == SensingState.AGGRESSIVE)
            {
                if (currentTarget && iffComponent.IsTargetHostile(currentTarget))
                {
                    if (TargetVisibile(currentTarget) && awarenessState != AwarenessState.ENGAGING)
                    {
                        Engage();
                    }

                    else if (TargetBehindObstacle(currentTarget) && awarenessState == AwarenessState.ENGAGING)
                    {
                        ChaseTarget();
                    }

                    else if (awarenessState == AwarenessState.SEARCHING && TargetLastKnownPositionReached())
                    {
                        CheckLastKnownPosition();
                        SearchForTargets();
                    }
                }

                else
                {
                    BecomeUnaware();
                    SearchForTargets();
                }
            }

            else if (sensingState == SensingState.CARELESS)
            {
                BecomeUnaware();
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        //Attack foe on collision
        if (iffComponent.IsTargetHostile(collision.gameObject.transform.root.gameObject))
        {
            OnShotReceived(collision.gameObject.transform.root.gameObject);
        }
    }
}
