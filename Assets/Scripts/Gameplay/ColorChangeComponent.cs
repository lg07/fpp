﻿using UnityEngine;

//Component handling material emissive color change on level state change
public class ColorChangeComponent : MonoBehaviour, IStateDependent
{
    public LevelDataScriptableObject levelData;

    [SerializeField]
    int emissiveMaterialIndex = 0;
    public int EmissiveMaterialIndex { get { return emissiveMaterialIndex; } }

    [SerializeField]
    bool isHackingDevice = false;

    MeshRenderer meshRenderer = null;
    LineRenderer lineRenderer = null;

    public void InitializeState()
    {
        switch (LevelController.Instance.GetLevelState)
        {
            case LevelController.LevelState.BLUE:
                {
                    ChangeState(LevelController.LevelState.BLUE);
                    break;
                }
            case LevelController.LevelState.RED:
                {
                    ChangeState(LevelController.LevelState.RED);
                    break;
                }
        }
    }

    public void ChangeState(LevelController.LevelState state)
    {
        switch (state)
        {
            case LevelController.LevelState.BLUE:
                {
                    if (!isHackingDevice) meshRenderer.materials[emissiveMaterialIndex].SetColor("_EmissionColor", levelData.stateColors[0]);
                    else if (meshRenderer) meshRenderer.materials[emissiveMaterialIndex].SetColor("_EmissionColor", levelData.stateColors[1]);
                    else if (lineRenderer) lineRenderer.materials[emissiveMaterialIndex].SetColor("_EmissionColor", levelData.stateColors[1]);

                    break;
                }
            case LevelController.LevelState.RED:
                {
                    if (!isHackingDevice) meshRenderer.materials[emissiveMaterialIndex].SetColor("_EmissionColor", levelData.stateColors[1]);
                    else if (meshRenderer) meshRenderer.materials[emissiveMaterialIndex].SetColor("_EmissionColor", levelData.stateColors[2]);
                    else if (lineRenderer) lineRenderer.materials[emissiveMaterialIndex].SetColor("_EmissionColor", levelData.stateColors[2]);

                    break;
                }
        }
    }

    void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        if (!meshRenderer) lineRenderer = GetComponent<LineRenderer>();
    }

    void OnEnable()
    {
        LevelController.SwitchLevelStateEvent += ChangeState;
        InitializeState();
    }

    void OnDisable()
    {
        LevelController.SwitchLevelStateEvent -= ChangeState;
    }
}
