﻿using System;
using UnityEngine;

public abstract class Health : MonoBehaviour
{
    public abstract void ReceiveDamage(float damage, GameObject damageInstigator);

    public static event Action<GameObject> OnKilledEvent;
    public void OnKilled()
    {
        if (OnKilledEvent != null) OnKilledEvent(gameObject);
    }
}
