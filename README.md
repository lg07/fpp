Red is Dead is an experimental FPP game (prototype), currently developed in __Unity 2022.2.14f1__.

The latest playable build can be downloaded at https://lg7.itch.io/red-is-dead

Technical Overview:

1.  At game startup, the Initializer class loads persistent GameManager prefab with managing classes added as components.
2.  The core gameplay loop is handled in the LevelController class.
3.  The key aspect of the core loop is current level state (Blue or Red). The effects of Blue and Red states are handled by the IStateDependent interface.
4.  AI-controlled probes' attitude towards player can be set to hostile or friendly via the IFF component.
5.  The project has a custom localization system based on JSON files, managed by the LocalizationManager component attached to the persistent GameManager prefab. Localization system basic scenarios are covered by automated tests (LocalizationTests class).
6.  In-game player UI is stored in a separate scene and loaded additively in LevelController.